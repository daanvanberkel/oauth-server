<?php
namespace Daanvanberkel;

use Daanvanberkel\Oauth\Repositories\AccessTokenRepository;
use Daanvanberkel\Oauth\Repositories\AuthCodeRepository;
use Daanvanberkel\Oauth\Repositories\ClientRepository;
use Daanvanberkel\Oauth\Repositories\RefreshTokenRepository;
use Daanvanberkel\Oauth\Repositories\ScopeRepository;
use Daanvanberkel\Oauth\Repositories\UserRepository;
use DateInterval;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

/**
 * Class AuthorizationServer
 * @package     Daanvanberkel
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AuthorizationServer {
    private static $instance;

    private static $accessTokenRespository;

    private static $public_key;

    /**
     * Get AuthorizationServer instance (singleton)
     *
     * @return \League\OAuth2\Server\AuthorizationServer
     */
    public static function getInstance(): \League\OAuth2\Server\AuthorizationServer {
        if (!isset(self::$instance) || !(self::$instance instanceof \League\OAuth2\Server\AuthorizationServer)) {
            $clientRepository = new ClientRepository();
            $scopeRepository = new ScopeRepository();

            if (!isset(self::$accessTokenRespository) || !(self::$accessTokenRespository instanceof AccessTokenRepositoryInterface)) {
                self::$accessTokenRespository = new AccessTokenRepository();
            }

            $accessTokenRepository = self::$accessTokenRespository;
            $authCodeRepository = new AuthCodeRepository();
            $refreshTokenRepository = new RefreshTokenRepository();

            $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'oauth');

            $privateKey = 'file://' . $path . '/private.key';

            if (!isset(self::$public_key)) {
                self::$public_key = 'file://' . $path . '/public.key';
            }

            $publicKey = self::$public_key;

            $server = new \League\OAuth2\Server\AuthorizationServer(
                $clientRepository,
                $accessTokenRepository,
                $scopeRepository,
                $privateKey,
                $publicKey
            );

            $authCodeGrant = new AuthCodeGrant(
                $authCodeRepository,
                $refreshTokenRepository,
                new DateInterval('PT10M') // Auth code is valid for 10 minutes
            );
            $authCodeGrant->setRefreshTokenTTL(new DateInterval('P1M')); // Refresh token is valid for 1 month

            $userRepository = new UserRepository();

            $passwordGrant = new PasswordGrant(
                $userRepository,
                $refreshTokenRepository
            );
            $passwordGrant->setRefreshTokenTTL(new DateInterval('P1M')); // Refresh token is valid for 1 month

            $refreshTokenGrant = new RefreshTokenGrant($refreshTokenRepository);
            $refreshTokenGrant->setRefreshTokenTTL(new DateInterval('P1M')); // Refresh token valid for 1 month

            $server->enableGrantType($authCodeGrant, new DateInterval('PT1H')); // Access token is valid for 1 hour
            $server->enableGrantType($passwordGrant, new DateInterval('PT1H')); // Access token is valid for 1 hour
            $server->enableGrantType($refreshTokenGrant, new DateInterval('PT1H')); // Access token is valid for 1 hour

            self::$instance = $server;
        }

        return self::$instance;
    }

    public static function getAccessTokenRepository() {
        if (!isset(self::$accessTokenRespository) || !(self::$accessTokenRespository instanceof AccessTokenRepositoryInterface)) {
            self::$accessTokenRespository = new AccessTokenRepository();
        }

        return self::$accessTokenRespository;
    }

    public static function getPublicKey() {
        if (!isset(self::$public_key)) {
            $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'oauth');
            self::$public_key = 'file://' . $path . '/public.key';
        }

        return self::$public_key;
    }
}