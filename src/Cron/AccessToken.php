<?php
namespace Daanvanberkel\Cron;

use Daanvanberkel\Db;

/**
 * Class AccessToken
 *
 * @package     Daanvanberkel\Cron
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AccessToken {
    private $pdo;

    public function __construct() {
        $this->_init();
    }

    public function __invoke() {
        $this->_init();
    }

    /**
     * Initiate this class
     */
    private function _init() {
        $this->pdo = Db::getPdo();

        $this->_deleteExpiredTokens();
    }


    /**
     * Delete expired accesstokens
     */
    private function _deleteExpiredTokens() {
        $stmt = $this->pdo->prepare("
            DELETE FROM
                accesstoken
            WHERE
                NOW() > expirydatetime OR 
                revoked = 1
        ");
        if (!$stmt->execute()) {
            print _("Deleting old accesstokens failed:") . "\n";
            print implode(PHP_EOL, $stmt->errorInfo()) . "\n";
            return;
        }

        $count = $stmt->rowCount();

        switch($count) {
            case 0:
                print _("No old accesstokens are deleted") . "\n";
                break;

            case 1:
                print _("One old accesstoken is deleted") . "\n";
                break;

            default:
                print sprintf(_("%d old accesstokens are deleted"), $count) . "\n";
                break;
        }
    }
}