<?php
namespace Daanvanberkel\Cron;

use Daanvanberkel\Db;

/**
 * Class Recovery
 * @package     Daanvanberkel\Cron
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Recovery {
    private $pdo;

    public function __construct() {
        $this->_init();
    }

    public function __invoke() {
        $this->_init();
    }

    /**
     * Initiate this class
     */
    private function _init() {
        $this->pdo = Db::getPdo();

        $this->_deleteExpiredTokens();
    }

    /**
     * Delete expires recoverytokens
     */
    private function _deleteExpiredTokens() {
        $stmt = $this->pdo->prepare("
            DELETE FROM
                recoverytoken
            WHERE
                NOW() > expire_date OR 
                revoked = 1
        ");
        if (!$stmt->execute()) {
            print _("Deleting old recoverytokens failed:") . "\n";
            print implode(PHP_EOL, $stmt->errorInfo()) . "\n";
            return;
        }

        $count = $stmt->rowCount();

        switch($count) {
            case 0:
                print _("No old recoverytokens are deleted") . "\n";
                break;

            case 1:
                print _("One old recoverytoken is deleted") . "\n";
                break;

            default:
                print sprintf(_("%d old recoverytokens are deleted") , $count) . "\n";
                break;
        }
    }
}