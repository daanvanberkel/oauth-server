<?php
namespace Daanvanberkel\Cron;

use Daanvanberkel\Db;

/**
 * Class RefreshToken
 *
 * @package     Daanvanberkel\Cron
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RefreshToken {
    private $pdo;

    public function __construct() {
        $this->_init();
    }

    public function __invoke() {
        $this->_init();
    }

    /**
     * Initiate this class
     */
    private function _init() {
        $this->pdo = Db::getPdo();

        $this->_deleteExpiredTokens();
    }


    /**
     * Delete expired authcodes
     */
    private function _deleteExpiredTokens() {
        $stmt = $this->pdo->prepare("
            DELETE FROM
                refreshtoken
            WHERE
                NOW() > expire_date OR 
                revoked = 1
        ");
        if (!$stmt->execute()) {
            print _("Deleting old refreshtokens failed:") . "\n";
            print implode(PHP_EOL, $stmt->errorInfo()) . "\n";
            return;
        }

        $count = $stmt->rowCount();

        switch($count) {
            case 0:
                print _("No old refreshtokens are deleted") . "\n";
                break;

            case 1:
                print _("One old refreshtoken is deleted") . "\n";
                break;

            default:
                print sprintf(_("%d old refreshtokens are deleted"), $count) . "\n";
                break;
        }
    }
}