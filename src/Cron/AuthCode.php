<?php
namespace Daanvanberkel\Cron;

use Daanvanberkel\Db;

/**
 * Class AuthCode
 *
 * @package     Daanvanberkel\Cron
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AuthCode {
    private $pdo;

    public function __construct() {
        $this->_init();
    }

    public function __invoke() {
        $this->_init();
    }

    /**
     * Initiate this class
     */
    private function _init() {
        $this->pdo = Db::getPdo();

        $this->_deleteExpiredTokens();
    }


    /**
     * Delete expired authcodes
     */
    private function _deleteExpiredTokens() {
        $stmt = $this->pdo->prepare("
            DELETE FROM
                authcode
            WHERE
                NOW() > expire_date OR 
                revoked = 1
        ");
        if (!$stmt->execute()) {
            print _("Deleting old authcodes failed:") . "\n";
            print implode(PHP_EOL, $stmt->errorInfo()) . "\n";
            return;
        }

        $count = $stmt->rowCount();

        switch($count) {
            case 0:
                print _("No old authcodes are deleted") . "\n";
                break;

            case 1:
                print _("One old authcode is deleted") . "\n";
                break;

            default:
                print sprintf(_("%d old authcodes are deleted"), $count) . "\n";
                break;
        }
    }
}