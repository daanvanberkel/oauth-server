<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Traits\Endpoint;
use League\OAuth2\Server\Exception\OAuthServerException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class AccessToken
 * @package     Daanvanberkel\Endpoints
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AccessToken {
    use Endpoint;

    /**
     * AccessToken post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        // Try to convert authcode into an accesstoken
        try {
            return $this->container['oauth_server']->respondToAccessTokenRequest($request, $response);
        } catch (OAuthServerException $exception) {
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }
    }
}