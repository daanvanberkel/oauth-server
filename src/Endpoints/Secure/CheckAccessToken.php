<?php
namespace Daanvanberkel\Endpoints\Secure;

use Slim\Http\Request;
use Slim\Http\Response;

class CheckAccessToken {
    public function get(Request $request, Response $response): Response {
        return $response->withJson(array("success" => true));
    }
}