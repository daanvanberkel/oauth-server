<?php
namespace Daanvanberkel\Endpoints\Secure;

use Daanvanberkel\Oauth\Repositories\UserRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class User {
    public function get(Request $request, Response $response, array $args) {
        $scopes = (array) $request->getAttribute('oauth_scopes');

        if (!in_array('name', $scopes) || !in_array('email', $scopes)) {
            return $response->withJson(array("success" => false, "error" => _("Client has not requested the right scopes (name and email are required)")), 401);
        }

        $userRepository = new UserRepository();

        try {
            $user = $userRepository->getUserEntityByIdentifier((int) $request->getAttribute('oauth_user_id'));
        } catch (\Exception $e) {
            return $response->withJson(array("success" => false, "error" => $e->getMessage()), 500);
        }

        return $response->withJson(array(
            "success" => true,
            "user" => array(
                "id_user" => $user->getIdentifier(),
                "firstname" => $user->getFirstname(),
                "lastname" => $user->getLastname(),
                "email" => $user->getEmailaddress()
            )
        ));
    }
}