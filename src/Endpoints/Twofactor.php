<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Repositories\RememberTokenRepository;
use Daanvanberkel\Traits\Endpoint;
use Daanvanberkel\TwoFactor\TwoFactorAuth;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

class Twofactor {
    use Endpoint;

    const CODE_MISSING = 1;
    const CODE_MISMATCH = 2;

    /**
     * Two factor get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        $authRequest = unserialize($_SESSION['authRequest']);

        $user = $authRequest->getUser();

        // If the user has two factor auth not setup show a message to setup two factor auth
        if (empty($user->getTwofactorSecret())) {
            return $this->container['renderer']->render($response, 'twofactor_message.php', array("title" => _("Two factor")));
        }

        $error = (int) ($_SESSION['twofactor_error'] ?? null);
        unset($_SESSION['twofactor_error']);

        // Show two factor verify code form
        return $this->container['renderer']->render($response, 'twofactor.php', array("title" => _("Two factor"), "error" => $error));
    }

    /**
     * Two factor post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        // Get code from submitted form
        $code = $request->getParsedBodyParam('code');

        if (empty($code)) {
            $_SESSION['twofactor_error'] = self::CODE_MISSING;
            return $response->withRedirect('/twofactor');
        }

        // Remove spaces from code
        $code = str_replace(' ', '', $code);

        $authRequest = unserialize($_SESSION['authRequest']);
        $user = $authRequest->getUser();

        // Verifiy code
        if (TwoFactorAuth::getInstance()->verifyCode($user->getTwofactorSecret(), $code) === false) {
            $_SESSION['twofactor_error'] = self::CODE_MISMATCH;
            return $response->withRedirect('/twofactor');
        }

        $user->setTwofactorVerified(true);

        if (isset($_SESSION['twofactor_remember']) && $_SESSION['twofactor_remember'] == true) {
            $tokenRepo = new RememberTokenRepository();

            try {
                $token = $tokenRepo->newRememberToken($user);

                setcookie(COOKIE_REMEMBER_IDENTIFIER, $token->getIdentifier(), time()+60*60*24*365, '/', 'oauth.daanvanberkel.nl', true, true);
                setcookie(COOKIE_REMEMBER_HASH, password_hash($token->getIdentifier() . $user->getEmailaddress() . $token->getIdentifier(), PASSWORD_DEFAULT), time()+60*60*24*365, '/', 'oauth.daanvanberkel.nl', true, true);
            } catch (Exception $exception) {
                $body = new Stream(fopen('php://temp', 'r+'));
                $body->write($exception->getMessage());
                return $response->withStatus(500)->withBody($body);
            }

            unset($_SESSION['twofactor_remember']);
        }

        $authRequest->setUser($user);

        $_SESSION['authRequest'] = serialize($authRequest);

        // Redirect user to redirect page, default: /approve
        $redirect = $_SESSION['twofactor_redirect'] ?? '/approve';
        unset($_SESSION['twofactor_redirect']);

        return $response->withRedirect($redirect);
    }
}