<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Repositories\RememberTokenRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class Logout {
    public function get(Request $request, Response $response, array $args) {
        if (isset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]) && isset($_COOKIE[COOKIE_REMEMBER_HASH])) {
            $tokenRepo = new RememberTokenRepository();

            try {
                $token = $tokenRepo->getTokenByIdentifier($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]);

                $tokenRepo->revokeToken($token);
            } catch (\Exception $exception) {
                return $response->withStatus(500)->write(_("An error occured while signing off"));
            }
        }

        unset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]);
        unset($_COOKIE[COOKIE_REMEMBER_HASH]);
        session_destroy();

        setcookie(COOKIE_REMEMBER_IDENTIFIER, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);
        setcookie(COOKIE_REMEMBER_HASH, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);
        return $response->withRedirect('/');
    }
}