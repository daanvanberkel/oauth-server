<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Repositories\AccessTokenRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class Revoked {
    public function get(Request $request, Response $response, array $args): Response {
        $tokenId = $args['id'];

        if (empty($tokenId)) {
            return $response->withJson(array("success" => true, "revoked" => true));
        }

        $accessTokenRepository = new AccessTokenRepository();

        if ($accessTokenRepository->isAccessTokenRevoked($tokenId) === true) {
            return $response->withJson(array("success" => true, "revoked" => true));
        }

        return $response->withJson(array("success" => true, "revoked" => false));
    }
}