<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Exceptions\RecoveryTokenException;
use Daanvanberkel\Oauth\Exceptions\UserException;
use Daanvanberkel\Oauth\Repositories\RecoveryTokenRepository;
use Daanvanberkel\Oauth\Repositories\UserRepository;
use Daanvanberkel\Traits\Endpoint;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class Recovery
 * @package     Daanvanberkel\Endpoints
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Recovery {
    use Endpoint;

    const CREDENTIALS_NOT_VALID = 1;
    const MAIL_FAILED = 2;
    const PASSWORDS_MISMATCH = 3;
    const MAIL_SEND = 4;
    const PASSWORD_CHANGED = 5;

    /**
     * Password recovery get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_PASSWORD_RECOVERY') != "true") {
            return $response->withRedirect('/login');
        }

        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        $error = (int) ($_SESSION['recovery_error'] ?? null);

        unset($_SESSION['recovery_error']);

        // Show password recovery form
        return $this->container['renderer']->render($response, 'recovery.php', array("title" => _('Forgot your password'), "error" => $error));
    }

    /**
     * Password recovery post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_PASSWORD_RECOVERY') != "true") {
            return $response->withRedirect('/login');
        }

        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        $username = $request->getParsedBodyParam('username');
        $email = $request->getParsedBodyParam('email');

        if (empty($username) || empty($email)) {
            $_SESSION['recovery_error'] = self::CREDENTIALS_NOT_VALID;
            return $response->withRedirect('/recovery');
        }

        $userRepository = new UserRepository();
        $recoveryTokenRepository = new RecoveryTokenRepository();

        // Try to get user entity with username and email
        try {
            $user = $userRepository->getUserEntityByUsernameAndEmail($username, $email);
        } catch (UserException $exception) {
            if ($exception->getCode() == UserException::NOT_FOUND) {
                // If the user is not found, don't let the user know! This is more secure
                $_SESSION['recovery_success'] = self::MAIL_SEND;
                return $response->withRedirect('/login');
            }

            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        // Try to create a new recovery token and save the token to the database
        try {
            $recoveryToken = $recoveryTokenRepository->saveNewToken(unserialize($_SESSION['authRequest']), $user);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        $email = sprintf(_("
Dear %s,\n
\n
To set a new password go to https://oauth.daanvanberkel.nl/recovery/%s\n
\n
If you didn't request a new password, you can ignore this email.\n
\n
Yours sincerely,\n
\n
Team oauth.daanvanberkel.nl service\n
"), $user->getName(), $recoveryToken);

        // Try to send an email with the recovery token to the user
        if (mail($user->getEmailaddress(), _("Recover password"), $email) === false) {
            $_SESSION['recovery_error'] = self::MAIL_FAILED;
            return $response->withRedirect('/recovery');
        }

        // Redirect user to the login page
        $_SESSION['recovery_success'] = self::MAIL_SEND;
        return $response->withRedirect('/login');
    }

    /**
     * Change password get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function changePasswordGet(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_PASSWORD_RECOVERY') != "true") {
            return $response->withRedirect('/login');
        }

        // Get token from url part
        $token = $args['token'];

        if (empty($token)) {
            return $response->withStatus(400)->write(_("Token is not valid"));
        }

        $recoveryTokenRepository = new RecoveryTokenRepository();

        // Try to get token in the database
        try {
            $recoveryTokenEntity = $recoveryTokenRepository->getToken($token);
        } catch (RecoveryTokenException $exception) {
            if ($exception->getCode() == RecoveryTokenException::NOT_FOUND) {
                return $response->withStatus(400)->write(_("Token is not valid"));
            }

            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        // Check if the token is not already used, or has expired
        if ($recoveryTokenEntity->isRevoked() || $recoveryTokenEntity->isExpired()) {
            return $response->withStatus(400)->write(_("Token is not valid anymore. <a href=\"\\recovery\">Request a new one.</a>"));
        }

        $error = (int) ($_SESSION['recovery_error'] ?? null);

        unset($_SESSION['recovery_error']);

        // Show password change form
        return $this->container['renderer']->render($response, 'recovery_change_password.php', array("title" => _("Forgot your password"), "token" => $recoveryTokenEntity->getIdentifier(), "error" => $error));
    }

    /**
     * Change password post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function changePasswordPost(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_PASSWORD_RECOVERY') != "true") {
            return $response->withRedirect('/login');
        }

        // Get submitted form data
        $token = $request->getParsedBodyParam('token');
        $password1 = $request->getParsedBodyParam('password1');
        $password2 = $request->getParsedBodyParam('password2');

        if (
            empty($token) ||
            empty($password1) ||
            empty($password2)
        ) {
            $_SESSION['recovery_error'] = self::CREDENTIALS_NOT_VALID;
            return $response->withRedirect('/recovery');
        }

        // The two passwords must match
        if ($password1 != $password2) {
            $_SESSION['recovery_error'] = self::PASSWORDS_MISMATCH;
            return $response->withRedirect('/recovery/' . $token);
        }

        $recoveryTokenRepository = new RecoveryTokenRepository();

        // Try to get token from database
        try {
            $recoveryTokenEntity = $recoveryTokenRepository->getToken($token);
        } catch (RecoveryTokenException $exception) {
            if ($exception->getCode() == RecoveryTokenException::NOT_FOUND) {
                return $response->withStatus(400)->write(_("Token is not valid"));
            }

            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        // Check if the token is not already used, or has expired
        if ($recoveryTokenEntity->isRevoked() || $recoveryTokenEntity->isExpired()) {
            return $response->withStatus(400)->write(_("Token is not valid"));
        }

        $userRepository = new UserRepository();

        // Try to get user from database and change password
        try {
            $user = $userRepository->getUserEntityByIdentifier($recoveryTokenEntity->getUserIdentifier());

            $userRepository->changePassword($user, $password1);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        $email = sprintf(_("
Dear %s,\n
\n
Your password is recently changed. If it was you who changed the password you can ignore this email.\n
Otherwise you van reply to this email to change the password.\n
\n
Yours sincerely,\n
\n
Team oauth.daanvanberkel.nl service\n
"), $user->getName());

        mail($user->getEmailaddress(), _("Password changed"), $email);

        $recoveryTokenEntity = $recoveryTokenRepository->revoke($recoveryTokenEntity);

        $_SESSION['recovery_success'] = self::PASSWORD_CHANGED;
        $_SESSION['authRequest'] = serialize($recoveryTokenEntity->getAuthRequest());
        return $response->withRedirect('/login');
    }
}