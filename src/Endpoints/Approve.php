<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Repositories\ApprovedClientRepository;
use Daanvanberkel\Traits\Endpoint;
use League\OAuth2\Server\Exception\OAuthServerException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class Approve
 * @package     Daanvanberkel\Endpoints
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Approve {
    use Endpoint;

    /**
     * Approve scopes get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        // Get Authorize request from session
        $authRequest = unserialize($_SESSION['authRequest']);

        // Get requested scopes
        $scopes = $authRequest->getScopes();

        // Get client (application)
        $application = $authRequest->getClient();

        // Get user
        $user = $authRequest->getUser();

        // Check if the user has two factor authentication setup and is verified, otherwise redirect user to two factor page
        if (!empty($user->getTwofactorSecret()) && $user->getTwofactorVerified() === false) {
            return $response->withRedirect('/twofactor');
        }

        $approvedRepo = new ApprovedClientRepository();

        try {
            $is_approved = $approvedRepo->isApprovedClient($application, $user, $scopes);

            if ($is_approved) {
                $authRequest->setAuthorizationApproved(true);

                return $this->container['oauth_server']->completeAuthorizationRequest($authRequest, $response);
            }
        } catch (\Exception $exception) {

        }

        // Show approve scopes page
        return $this->container['renderer']->render($response, 'approve.php', array(
            "title" => "Goedkeuren",
            "scopes" => $scopes,
            "application" => $application,
            "user" => $user
        ));
    }

    /**
     * Approve scopes post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        $action = $request->getParsedBodyParam("action");
        $approve = (bool) $request->getParsedBodyParam('approve', false);

        if (empty($action)) {
            return $response->withRedirect('/approve');
        }

        // Check what button is clicked
        $approved = ($action == "approve" ? true : false);

        $authRequest = unserialize($_SESSION['authRequest']);

        if ($approve === true && $approved === true) {
            $approveRepo = new ApprovedClientRepository();

            // Get requested scopes
            $scopes = $authRequest->getScopes();

            // Get client (application)
            $client = $authRequest->getClient();

            // Get user
            $user = $authRequest->getUser();

            try {
                $approveRepo->newApprovedClient($client, $user, $scopes);
            } catch (OAuthServerException $exception) {
                return $exception->generateHttpResponse($response);
            } catch (\Exception $exception) {
                $body = new Stream(fopen('php://temp', 'r+'));
                $body->write($exception->getMessage());
                return $response->withStatus(500)->withBody($body);
            }
        }

        // Is the user accepted the scopes, send a authcode to the client, otherwise send an error message to the client.
        try {
            $authRequest->setAuthorizationApproved($approved);

            return $this->container['oauth_server']->completeAuthorizationRequest($authRequest, $response);
        } catch (OAuthServerException $exception) {
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }
    }
}