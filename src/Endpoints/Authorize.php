<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Traits\Endpoint;
use League\OAuth2\Server\Exception\OAuthServerException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class Authorize
 * @package     Daanvanberkel\Endpoints
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Authorize {
    use Endpoint;

    /**
     * Authorize get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        // Try to validate authorize request
        try {
            $authRequest = $this->container['oauth_server']->validateAuthorizationRequest($request);

            $serialized = serialize($authRequest);

            $_SESSION['authRequest'] = $serialized;

            return $response->withRedirect('/login');
        } catch (OAuthServerException $exception) {
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }
    }
}