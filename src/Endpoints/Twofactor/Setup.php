<?php
namespace Daanvanberkel\Endpoints\Twofactor;

use Daanvanberkel\Oauth\Repositories\UserRepository;
use Daanvanberkel\Traits\Endpoint;
use Daanvanberkel\TwoFactor\TwoFactorAuth;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class Setup
 * @package     Daanvanberkel\Endpoints\Twofactor
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Setup {
    use Endpoint;

    const CODE_MISSING = 1;
    const CODE_NOT_VALID = 2;
    const SECRET_MISSING = 3;

    /**
     * Two factor setup get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        $secret = $_SESSION['twofactor_secret'] ?? null;
        $error = $_SESSION['twofactor_error'] ?? null;

        unset($_SESSION['twofactor_secret']);
        unset($_SESSION['twofactor_error']);

        // Create new secret is the is not one set in the session
        if (empty($secret)) {
            $secret = TwoFactorAuth::getInstance()->createSecret();
        }

        $authRequest = unserialize($_SESSION['authRequest']);

        $user = $authRequest->getUser();

        // Generate QR-code for the user to scan
        $qrcode = TwoFactorAuth::getInstance()->getQRCodeImageAsDataUri($user->getEmailaddress(), $secret);

        // Show page
        return $this->container['renderer']->render($response, 'twofactor_setup.php', array("title" => _("Twofactory"), "qr_code" => $qrcode, "secret" => $secret, "error" => $error));
    }

    /**
     * Two factor setup post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest() === false) {
            return $response->withRedirect('/authorize');
        }

        $code = $request->getParsedBodyParam('code');
        $secret = $request->getParsedBodyParam('secret');

        if (empty($secret)) {
            $_SESSION['twofactor_error'] = self::SECRET_MISSING;
            return $response->withRedirect('/twofactor/setup');
        }

        if (empty($code)) {
            $_SESSION['twofactor_secret'] = $secret;
            $_SESSION['twofactor_error'] = self::CODE_MISSING;
            return $response->withRedirect('/twofactor/setup');
        }

        // Remove spaces from verification code
        $code = str_replace(' ', '', $code);

        $authRequest = unserialize($_SESSION['authRequest']);

        $user = $authRequest->getUser();

        // Check if the entered code is valid
        if (TwoFactorAuth::getInstance()->verifyCode($secret, $code) === false) {
            $_SESSION['twofactor_secret'] = $secret;
            $_SESSION['twofactor_error'] = self::CODE_NOT_VALID;
            return $response->withRedirect('/twofactor/setup');
        }

        // Save secret in the database
        $user->setTwofactorSecret($secret);

        $userRepository = new UserRepository();

        try {
            $user = $userRepository->saveNewTwofactorSecret($user, $secret);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        $email = sprintf(_("
Dear %s,\n
\n
For your account at oauth.daanvanberkel.nl is tow factor authentication now active.\n
\n
Your Sincerely,\n
\n
Team oauth.daanvanberkel.nl service\n
"), $user->getName());

        // Inform the user about tow factor setup
        mail($user->getEmailaddress(), _("Twofactor authentication activated"), $email);

        $user->setTwofactorVerified(true);

        $authRequest->setUser($user);

        $_SESSION['authRequest'] = serialize($authRequest);

        // Redirect user to /approve page
        return $response->withRedirect('/approve');
    }
}