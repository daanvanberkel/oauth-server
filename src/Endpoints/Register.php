<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Repositories\UserRepository;
use Daanvanberkel\Traits\Endpoint;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

class Register {
    use Endpoint;

    const DATA_MISSING = 1;
    const EMAIL_NOT_VALID = 2;
    const PASSWORD_MISMATCH = 3;
    const USERNAME_OR_EMAIL_IN_USE = 4;

    /**
     * Register get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     *
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_REGISTRATION') != 'true') {
            return $response->withRedirect('/login');
        }

        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        $error = (int) ($_SESSION['register_error'] ?? null);

        unset($_SESSION['register_error']);

        return $this->container['renderer']->render($response, 'register.php', array("title" => _("Register"), "error" => $error));
    }

    /**
     * Register post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     *
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if (getenv('ENABLE_REGISTRATION') != 'true') {
            return $response->withRedirect('/login');
        }

        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        // Get form data from request
        $username = $request->getParsedBodyParam('username');
        $firstname = $request->getParsedBodyParam('firstname');
        $lastname = $request->getParsedBodyParam('lastname');
        $email = $request->getParsedBodyParam('email');
        $password1 = $request->getParsedBodyParam('password1');
        $password2 = $request->getParsedBodyParam('password2');

        if (
            empty($username) ||
            empty($firstname) ||
            empty($lastname) ||
            empty($email) ||
            empty($password1) ||
            empty($password2)
        ) {
            $_SESSION['register_error'] = self::DATA_MISSING;
            return $response->withRedirect('/register');
        }

        // Check if email is valid
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['register_error'] = self::EMAIL_NOT_VALID;
            return $response->withRedirect('/register');
        }

        // Check if passwords match
        if ($password1 != $password2) {
            $_SESSION['register_error'] = self::PASSWORD_MISMATCH;
            return $response->withRedirect('/register');
        }

        $userRepository = new UserRepository();

        // Check if username or email is already in use
        try {
            $usernameInUse = $userRepository->isUsernameInUse($username);
            $emailInUse = $userRepository->isEmailInUse($email);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        if ($usernameInUse === true || $emailInUse === true) {
            $_SESSION['register_error'] = self::USERNAME_OR_EMAIL_IN_USE;
            return $response->withRedirect('/register');
        }

        // Save new user to the database
        try {
            $user = $userRepository->newUser($username, $email, $password1, $firstname, $lastname);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        // Redirect user to login page
        $_SESSION['register_success'] = true;
        return $response->withRedirect('/login');
    }
}