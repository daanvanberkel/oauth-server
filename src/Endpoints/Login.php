<?php
namespace Daanvanberkel\Endpoints;

use Daanvanberkel\Oauth\Exceptions\UserException;
use Daanvanberkel\Oauth\Repositories\LoginTriesRepository;
use Daanvanberkel\Oauth\Repositories\RememberTokenRepository;
use Daanvanberkel\Oauth\Repositories\UserRepository;
use Daanvanberkel\Traits\Endpoint;
use Exception;
use League\OAuth2\Server\Exception\OAuthServerException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

/**
 * Class Login
 * @package     Daanvanberkel\Endpoints
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Login {
    use Endpoint;

    /**
     * Login get request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function get(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        if (isset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]) && isset($_COOKIE[COOKIE_REMEMBER_HASH])) {
            $tokenRepo = new RememberTokenRepository();
            $userRepo = new UserRepository();

            try {
                $token = $tokenRepo->getTokenByIdentifier($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]);
                $user = $userRepo->getUserEntityByIdentifier($token->getIdUser());

                if (!$tokenRepo->checkHash($token, $_COOKIE[COOKIE_REMEMBER_HASH])) {
                    unset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]);
                    unset($_COOKIE[COOKIE_REMEMBER_HASH]);

                    setcookie(COOKIE_REMEMBER_IDENTIFIER, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);
                    setcookie(COOKIE_REMEMBER_HASH, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);

                    $tokenRepo->revokeByUser($user);

                    return $response->write(_("Because of your safety you cannot be signed in automatically. <a href=\"/login\">Sign in</a>."));
                }

                $user->setTwofactorVerified(true);

                $authRequest = unserialize($_SESSION['authRequest']);

                try {
                    $authRequest->setUser($user);
                } catch (OAuthServerException $exception) {
                    return $exception->generateHttpResponse($response);
                } catch (\Exception $exception) {
                    $body = new Stream(fopen('php://temp', 'r+'));
                    $body->write($exception->getMessage());
                    return $response->withStatus(500)->withBody($body);
                }

                $_SESSION['authRequest'] = serialize($authRequest);

                return $response->withRedirect('/approve');
            } catch (Exception $exception) {
                unset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]);
                unset($_COOKIE[COOKIE_REMEMBER_HASH]);

                setcookie(COOKIE_REMEMBER_IDENTIFIER, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);
                setcookie(COOKIE_REMEMBER_HASH, null, time() - 1, '/', 'oauth.daanvanberkel.nl', true, true);
                return $response->write(_("An error occured while signing you in automatically. <a href=\"/login\">Sign in</a>."));
            }
        }

        $error = (int) ($_SESSION['login_error'] ?? null);
        $username = $_SESSION['login_username'] ?? null;
        $recovery = (int) ($_SESSION['recovery_success'] ?? null);
        $register = $_SESSION['register_success'] ?? null;
        $remember = (bool) ($_SESSION['login_remember'] ?? false);

        unset($_SESSION['login_username']);
        unset($_SESSION['login_error']);
        unset($_SESSION['recovery_success']);
        unset($_SESSION['register_success']);
        unset($_SESSION['login_remember']);

        $triesRepo = new LoginTriesRepository();

        try {
            $tries = $triesRepo->getTriesCount();
        } catch (Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        if ($tries >= 3) {
            $error = UserException::BRUTE_FORCE_ERROR;
        }

        // Show login page
        return $this->container['renderer']->render($response, 'inlog.php', array(
            "title" => _("Sign in"),
            "error" => $error,
            "username" => $username,
            "recovery" => $recovery,
            "register" => $register,
            "remember" => $remember,
            "tries" => $tries
        ));
    }

    /**
     * Login post request
     *
     * @param   Request     $request        PSR-7 request
     * @param   Response    $response       PSR-7 response
     * @param   array       $args           Url arguments
     * @return  Response                    PSR-7 response
     */
    public function post(Request $request, Response $response, array $args): Response {
        if ($this->_checkAuthrequest(false) === false) {
            return $response->withRedirect('/authorize');
        }

        $userRepository = new UserRepository();

        $username = $request->getParsedBodyParam('username');
        $password = $request->getParsedBodyParam('password');
        $remember = (bool) $request->getParsedBodyParam('remember', false);

        // Try to get user entity with the username and password
        try {
            $user = $userRepository->getUserEntityByUserCredentials($username, $password);
        } catch (UserException $exception) {
            $triesRepo = new LoginTriesRepository();

            try {
                $triesRepo->addTry($username);

                $total = $triesRepo->getTriesCount();

                if ($total >= 3) {
                    $_SESSION['login_error'] = UserException::BRUTE_FORCE_ERROR;

                    return $response->withRedirect('/login');
                }
            } catch (Exception $exception) {
                $body = new Stream(fopen('php://temp', 'r+'));
                $body->write($exception->getMessage());
                return $response->withStatus(500)->withBody($body);
            }

            if ($exception->getCode() == UserException::CREDENTIALS_NOT_VALID) {
                // Save username to session if password is wrong.
                $_SESSION['login_username'] = $username;
            }
            $_SESSION['login_error'] = $exception->getCode();
            $_SESSION['login_remember'] = $remember;

            return $response->withRedirect('/login');
        } catch (Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        if ($remember && empty($user->getTwofactorSecret())) {
            $tokenRepo = new RememberTokenRepository();

            try {
                $token = $tokenRepo->newRememberToken($user);

                setcookie(COOKIE_REMEMBER_IDENTIFIER, $token->getIdentifier(), time()+60*60*24*365, '/', 'oauth.daanvanberkel.nl', true, true);
                setcookie(COOKIE_REMEMBER_HASH, password_hash($token->getIdentifier() . $user->getEmailaddress() . $token->getIdentifier(), PASSWORD_DEFAULT), time()+60*60*24*365, '/', 'oauth.daanvanberkel.nl', true, true);
            } catch (Exception $exception) {
                $body = new Stream(fopen('php://temp', 'r+'));
                $body->write($exception->getMessage());
                return $response->withStatus(500)->withBody($body);
            }
        } elseif($remember) {
            $_SESSION['twofactor_remember'] = true;
        }

        $authRequest = unserialize($_SESSION['authRequest']);

        // Save user the authorize request
        try {
            $authRequest->setUser($user);
        } catch (OAuthServerException $exception) {
            return $exception->generateHttpResponse($response);
        } catch (\Exception $exception) {
            $body = new Stream(fopen('php://temp', 'r+'));
            $body->write($exception->getMessage());
            return $response->withStatus(500)->withBody($body);
        }

        $_SESSION['authRequest'] = serialize($authRequest);

        // Redirect user to two factor page
        return $response->withRedirect('/twofactor');
    }
}