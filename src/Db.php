<?php
namespace Daanvanberkel;

/**
 * Class Db
 * @package     Daanvanberkel
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class Db {
    private $pdo;
    private static $instance;

    /**
     * Db constructor.
     *
     * Get variables from .env file en initiate a new PDO object
     */
    public function __construct()
    {
        EnvVariables::getInstance();

        $this->pdo = new \PDO("mysql:host=" . getenv('MYSQL_HOST') . ';dbname=' . getenv('MYSQL_DB'), getenv('MYSQL_USER'), getenv('MYSQL_PASS'));
    }

    /**
     * Get PDO object (singleton)
     *
     * @return \PDO
     */
    public static function getPdo() {
        if (!isset(self::$instance) || !(self::$instance instanceof self)) {
            self::$instance = new self();
        }

        return self::$instance->pdo;
    }
}