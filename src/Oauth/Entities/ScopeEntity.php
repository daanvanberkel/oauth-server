<?php
namespace Daanvanberkel\Oauth\Entities;

use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

/**
 * Class ScopeEntity
 * @package     Daanvanberkel\Oauth\Entities
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class ScopeEntity implements ScopeEntityInterface
{
    use EntityTrait;

    private $description;

    public function setDescription(string $description) {
        $this->description = $description;
    }

    public function getDescription() {
        return $this->description;
    }

    public function jsonSerialize()
    {
        return $this->getIdentifier();
    }
}
