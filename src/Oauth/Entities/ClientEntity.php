<?php
namespace Daanvanberkel\Oauth\Entities;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\ClientTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

/**
 * Class ClientEntity
 * @package     Daanvanberkel\Oauth\Entities
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class ClientEntity implements ClientEntityInterface
{
    use EntityTrait, ClientTrait;

    protected $grant;

    /**
     * Set client name
     *
     * @param   $name
     * @return  ClientEntity
     */
    public function setName($name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Set redirect uri
     *
     * @param   $uri
     * @return  ClientEntity
     */
    public function setRedirectUri($uri): self {
        $this->redirectUri = $uri;
        return $this;
    }

    /**
     * Set grant type
     *
     * @param $grant
     *
     * @return ClientEntity
     */
    public function setGrant($grant): self {
        $this->grant = $grant;
        return $this;
    }

    /**
     * Get grant type
     *
     * @return null|string
     */
    public function getGrant(): ?string {
        return $this->grant;
    }
}
