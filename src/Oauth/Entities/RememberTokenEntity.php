<?php
namespace Daanvanberkel\Oauth\Entities;

class RememberTokenEntity {
    private $id_token = 0;
    private $id_user = 0;
    private $identifier = "";
    private $hash = "";
    private $revoked = false;

    public function setIdToken(int $id_token): self {
        $this->id_token = $id_token;
        return $this;
    }

    public function setIdUser(int $id_user): self {
        $this->id_user = $id_user;
        return $this;
    }

    public function setIdentifier(string $identifier): self {
        $this->identifier = $identifier;
        return $this;
    }

    public function setHash(string $hash): self {
        $this->hash = $hash;
        return $this;
    }

    public function setRevoked(bool $revoked): self {
        $this->revoked = $revoked;
        return $this;
    }

    public function getIdToken(): int {return $this->id_token;}
    public function getIdUser(): int {return $this->id_user;}
    public function getIdentifier(): string {return $this->identifier;}
    public function getHash(): string {return $this->hash;}
    public function getRevoked(): bool {return $this->isRevoked();}
    public function isRevoked(): bool {return $this->revoked;}
}