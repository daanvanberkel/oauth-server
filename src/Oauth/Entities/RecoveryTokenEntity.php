<?php
namespace Daanvanberkel\Oauth\Entities;

use DateTime;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;

/**
 * Class RecoveryTokenEntity
 * @package     Daanvanberkel\Oauth\Entities
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RecoveryTokenEntity {
    private $identifier;
    private $authRequest;
    private $revoked = false;
    private $expireDate;
    private $userIdentifier;

    public function getIdentifier() {
        return $this->identifier;
    }

    public function getAuthRequest(): AuthorizationRequest {
        return $this->authRequest;
    }

    public function getExpireDate(): DateTime {
        return $this->expireDate;
    }

    public function isRevoked(): bool {
        return (bool) $this->revoked;
    }

    public function isExpired(): bool {
        $now = new DateTime();
        return ($now > $this->expireDate);
    }

    public function getUserIdentifier() {
        return $this->userIdentifier;
    }

    public function setIdentifier($id): self {
        $this->identifier = $id;
        return $this;
    }

    public function setUserIdentifier($id): self {
        $this->userIdentifier = $id;
        return $this;
    }

    public function setAuthRequest($authRequest): self {
        if (is_string($authRequest)) {
            $authRequest = unserialize($authRequest);

            if ($authRequest === false || !($authRequest instanceof AuthorizationRequest)) {
                return $this;
            }

            $this->authRequest = $authRequest;
        }

        if ($authRequest instanceof AuthorizationRequest) {
            $this->authRequest = $authRequest;
        }

        return $this;
    }

    public function setRevoked($revoked): self {
        $this->revoked = (bool) $revoked;
        return $this;
    }

    public function setExpiredDate($date): self {
        if ($date instanceof DateTime) {
            $this->expireDate = $date;
            return $this;
        }

        try {
            $date = new DateTime($date);
        } catch (\Exception $e) {
            return $this;
        }

        $this->expireDate = $date;

        return $this;
    }
}