<?php
namespace Daanvanberkel\Oauth\Entities;

use League\OAuth2\Server\Entities\UserEntityInterface;

/**
 * Class UserEntity
 * @package     Daanvanberkel\Oauth\Entities
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class UserEntity implements UserEntityInterface {
    private $identifier;
    private $email;
    private $twofactor_secret;
    private $twofactor_verified = false;
    private $firstname;
    private $lastname;

    public function getIdentifier() {
        return $this->identifier;
    }

    public function getEmailaddress() {
        return $this->email;
    }

    public function getTwofactorSecret() {
        return $this->twofactor_secret;
    }

    public function getTwofactorVerified() {
        return $this->twofactor_verified;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getName() {
        return $this->getFirstname() . " " . $this->getLastname();
    }

    public function setIdentifier($id): self {
        $this->identifier = $id;
        return $this;
    }

    public function setEmailaddress($email): self {
        $this->email = $email;
        return $this;
    }

    public function setTwofactorSecret($twofactor_secret): self {
        $this->twofactor_secret = $twofactor_secret;
        return $this;
    }

    public function setTwofactorVerified(bool $status): self {
        $this->twofactor_verified = $status;
        return $this;
    }

    public function setFirstname($name): self {
        $this->firstname = $name;
        return $this;
    }

    public function setLastname($name): self {
        $this->lastname = $name;
        return $this;
    }
}
