<?php
namespace Daanvanberkel\Oauth\Exceptions;

/**
 * Class RecoveryTokenException
 * @package     Daanvanberkel\Oauth\Exceptions
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RecoveryTokenException extends \Exception {
    const NOT_FOUND = 1;
}