<?php
namespace Daanvanberkel\Oauth\Exceptions;

/**
 * Class LoginTriesException
 * @package     Daanvanberkel\Oauth\Exceptions
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class LoginTriesException extends \Exception {
    const NOT_FOUND = 1;
    const DB_ERROR = 2;
}