<?php
namespace Daanvanberkel\Oauth\Exceptions;

/**
 * Class UserException
 * @package     Daanvanberkel\Oauth\Exceptions
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class UserException extends \Exception {
    const NOT_FOUND = 1;
    const CREDENTIALS_NOT_VALID = 2;
    const BRUTE_FORCE_ERROR = 3;
}