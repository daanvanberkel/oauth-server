<?php
namespace Daanvanberkel\Oauth\Exceptions;

/**
 * Class RecoveryTokenException
 * @package     Daanvanberkel\Oauth\Exceptions
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RememberTokenException extends \Exception {
    const NOT_FOUND = 1;
    const DB_ERROR = 2;
}