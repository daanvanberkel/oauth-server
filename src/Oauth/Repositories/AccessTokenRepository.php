<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Daanvanberkel\Oauth\Entities\AccessTokenEntity;

/**
 * Class AccessTokenRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AccessTokenRepository implements AccessTokenRepositoryInterface {
    /**
     * Save accesstoken to database
     *
     * @param AccessTokenEntityInterface $accessTokenEntity
     *
     * @return AccessTokenEntityInterface
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("INSERT INTO accesstoken (identifier, id_client, id_user, expirydatetime) VALUES (:identifier, :id_client, :id_user, :expirydatetime)");
        if (!$stmt->execute(array(
            ":identifier" => $accessTokenEntity->getIdentifier(),
            ":id_client" => $accessTokenEntity->getClient()->getIdentifier(),
            ":id_user" => $accessTokenEntity->getUserIdentifier(),
            ":expirydatetime" => $accessTokenEntity->getExpiryDateTime()->format('Y-m-d H:i:s')
        ))) {
            return;
        }

        foreach($accessTokenEntity->getScopes() as $scope) {
            $stmt = $pdo->prepare("INSERT INTO accesstoken_scopes (id_accesstoken, id_scope) VALUES (:id_accesstoken, :id_scope)");
            $stmt->execute(array(
                ":id_accesstoken" => $accessTokenEntity->getIdentifier(),
                ":id_scope" => $scope->getIdentifier()
            ));
        }

        return $accessTokenEntity;
    }

    /**
     * Revoke accesstoken in the database
     *
     * @param string $tokenId
     *
     * @return string
     */
    public function revokeAccessToken($tokenId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("UPDATE accesstoken SET revoked = 1 WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $tokenId))) {
            return;
        }

        return $tokenId;
    }

    /**
     * Check if the accesstoken is revoked
     *
     * @param string $tokenId
     *
     * @return boolean
     */
    public function isAccessTokenRevoked($tokenId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("SELECT expirydatetime, revoked FROM accesstoken WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $tokenId))) {
            return;
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return true;
        }

        $expire = new \DateTime($result->expirydatetime);
        $now = new \DateTime();

        return (bool) (($now > $expire) ? true : $result->revoked);
    }

    /**
     * Get a new token
     *
     * @param ClientEntityInterface $clientEntity
     * @param array                 $scopes
     * @param null                  $userIdentifier
     *
     * @return AccessTokenEntity
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null) {
        $accessToken = new AccessTokenEntity();
        $accessToken->setClient($clientEntity);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }
        $accessToken->setUserIdentifier($userIdentifier);

        return $accessToken;
    }
}
