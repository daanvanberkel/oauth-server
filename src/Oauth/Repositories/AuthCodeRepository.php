<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use Daanvanberkel\Oauth\Entities\AuthCodeEntity;

/**
 * Class AuthCodeRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class AuthCodeRepository implements AuthCodeRepositoryInterface {
    /**
     * Save authcode in the database
     *
     * @param AuthCodeEntityInterface $authCodeEntity
     *
     * @return AuthCodeEntityInterface
     */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("INSERT INTO authcode (identifier, id_user, id_client, redirect_uri, expire_date) VALUES (:identifier, :id_user, :id_client, :redirect_uri, :expire_date)");
        if (!$stmt->execute(array(
            ":identifier" => $authCodeEntity->getIdentifier(),
            ":id_user" => $authCodeEntity->getUserIdentifier(),
            ":id_client" => $authCodeEntity->getClient()->getIdentifier(),
            ":redirect_uri" => $authCodeEntity->getRedirectUri(),
            ":expire_date" => $authCodeEntity->getExpiryDateTime()->format("Y-m-d H:i:s")
        ))) {
            return;
        }

        foreach($authCodeEntity->getScopes() as $scope) {
            $stmt = $pdo->prepare("INSERT INTO authcode_scopes (id_authcode, id_scope) VALUES (:id_authcode, :id_scope)");
            $stmt->execute(array(
                ":id_authcode" => $authCodeEntity->getIdentifier(),
                ":id_scope" => $scope->getIdentifier()
            ));
        }

        return $authCodeEntity;
    }

    /**
     * Revoke authcode in the database
     *
     * @param string $codeId
     *
     * @return string
     */
    public function revokeAuthCode($codeId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("UPDATE authcode SET revoked = 1 WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $codeId))) {
            return;
        }

        return $codeId;
    }

    /**
     * Check is authcode is revoked
     *
     * @param string $codeId
     *
     * @return boolean
     */
    public function isAuthCodeRevoked($codeId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("SELECT expire_date, revoked FROM authcode WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $codeId))) {
            return;
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return true;
        }

        $expire = new \DateTime($result->expire_date);
        $now = new \DateTime();

        return (bool) (($now > $expire) ? true : $result->revoked);
    }

    /**
     * Get authcodeentity
     *
     * @return AuthCodeEntity
     */
    public function getNewAuthCode() {
        return new AuthCodeEntity();
    }
}
