<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Daanvanberkel\Oauth\Entities\ClientEntity;

/**
 * Class ClientRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class ClientRepository implements ClientRepositoryInterface {
    /**
     * Get client entity from database
     *
     * @param string $clientIdentifier
     * @param string $grantType
     * @param null   $clientSecret
     * @param bool   $mustValidateSecret
     *
     * @return ClientEntity
     */
    public function getClientEntity($clientIdentifier, $grantType, $clientSecret = null, $mustValidateSecret = true) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("SELECT identifier, name, secret, redirect_url, grant_type FROM client WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $clientIdentifier))) {
            return;
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return;
        }

        if (
            $mustValidateSecret === true &&
            password_verify($clientSecret, $result->secret) === false
        ) {
            return;
        }

        $client = new ClientEntity();
        $client->setIdentifier($clientIdentifier);
        $client->setName($result->name);
        $client->setRedirectUri($result->redirect_url);
        $client->setGrant($result->grant_type);

        return $client;
    }
}
