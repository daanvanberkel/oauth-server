<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use Daanvanberkel\Oauth\Entities\ClientEntity;
use Daanvanberkel\Oauth\Entities\ScopeEntity;
use Daanvanberkel\Oauth\Entities\UserEntity;
use Daanvanberkel\Oauth\Exceptions\ApprovedClientException;

class ApprovedClientRepository {
    public function newApprovedClient(ClientEntity $client, UserEntity $user, array $scopes) {
        $stmt = Db::getPdo()->prepare("
            INSERT INTO
                approved_clients (
                    id_client,
                    id_user
                )
            VALUES (
                :id_client,
                :id_user
            )
        ");

        if (!$stmt->execute(array(
            ":id_client" => $client->getIdentifier(),
            ":id_user" => $user->getIdentifier()
        ))) {
            throw new ApprovedClientException(implode(PHP_EOL, $stmt->errorInfo()), ApprovedClientException::DB_ERROR);
        }

        $id_approved_client = Db::getPdo()->lastInsertId();

        foreach($scopes as $scope) {
            if (!($scope instanceof ScopeEntity)) {
                continue;
            }

            $stmt = Db::getPdo()->prepare("
                INSERT INTO
                    approved_client_scopes (
                        id_approved_client, 
                        id_scope
                    )
                VALUES (
                    :id_approved_client,
                    :id_scope
                )
            ");

            if (!$stmt->execute(array(
                ":id_approved_client" => $id_approved_client,
                ":id_scope" => $scope->getIdentifier()
            ))) {
                throw new ApprovedClientException(implode(PHP_EOL, $stmt->errorInfo()), ApprovedClientException::DB_ERROR);
            }
        }
    }

    public function isApprovedClient(ClientEntity $client, UserEntity $user, array $scopes): bool {
        $stmt = Db::getPdo()->prepare("
            SELECT
                a.id_approved_client,
                a.id_client,
                a.id_user,
                a.approve_date,
                a.revoked,
                b.id_scope
            FROM approved_clients AS a
            JOIN approved_client_scopes AS b ON a.id_approved_client = b.id_approved_client
            WHERE
                a.id_client = :id_client AND 
                a.id_user = :id_user AND 
                a.revoked = 0
        ");

        if (!$stmt->execute(array(
            ":id_client" => $client->getIdentifier(),
            ":id_user" => $user->getIdentifier()
        ))) {
            throw new ApprovedClientException(implode(PHP_EOL, $stmt->errorInfo()), ApprovedClientException::DB_ERROR);
        }

        $results = $stmt->fetchAll(\PDO::FETCH_OBJ);

        if ($results === false) {
            return false;
        }

        if (count($results) < count($scopes)) {
            return false;
        }

        $result_ids = array();

        foreach($results as $result) {
            if ($result->revoked == true) {
                return false;
            }

            $result_ids[] = $result->id_scope;
        }

        foreach($scopes as $scope) {
            if (!($scope instanceof ScopeEntity)) {
                continue;
            }

            if (!in_array($scope->getIdentifier(), $result_ids)) {
                return false;
            }
        }

        return true;
    }
}