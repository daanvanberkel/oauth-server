<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use Daanvanberkel\Oauth\Entities\RememberTokenEntity;
use Daanvanberkel\Oauth\Entities\UserEntity;
use Daanvanberkel\Oauth\Exceptions\RememberTokenException;

class RememberTokenRepository {
    public function newRememberToken(UserEntity $user): RememberTokenEntity {
        try {
            $identifier = $this->getUniqueToken();
        } catch (\Exception $exception) {
            throw $exception;
        }

        $hash = password_hash($identifier . $user->getEmailaddress() . $identifier, PASSWORD_DEFAULT);

        $stmt = Db::getPdo()->prepare("
            INSERT INTO
                remember_token (id_user, identifier)
            VALUES (
                :id_user,
                :identifier
            )
        ");

        if (!$stmt->execute(array(
            ":id_user" => $user->getIdentifier(),
            ":identifier" => $identifier
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        return (new RememberTokenEntity())
            ->setIdToken((int) Db::getPdo()->lastInsertId())
            ->setIdUser((int) $user->getIdentifier())
            ->setIdentifier($identifier)
            ->setHash($hash);
    }

    public function revokeToken(RememberTokenEntity $token): RememberTokenEntity {
        $stmt = Db::getPdo()->prepare("
            UPDATE
                remember_token
            SET
                revoked = 1
            WHERE
                id_token = :id_token
        ");

        if (!$stmt->execute(array(
            ":id_token" => $token->getIdToken()
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        $token->setRevoked(true);

        return $token;
    }

    public function isRevoked(RememberTokenEntity $token): bool {
        $stmt = Db::getPdo()->prepare("
            SELECT
                revoked
            FROM
                remember_token
            WHERE
                id_token = :id_token 
        ");

        if (!$stmt->execute(array(
            ":id_token" => $token->getIdToken()
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new RememberTokenException(_("Token not found"), RememberTokenException::NOT_FOUND);
        }

        return (bool) $result->revoked;
    }

    public function getTokenByIdentifier(string $identifier): RememberTokenEntity {
        $stmt = Db::getPdo()->prepare("
            SELECT
                id_token,
                id_user,
                identifier,
                revoked
            FROM
                remember_token
            WHERE
                identifier = :identifier
        ");

        if (!$stmt->execute(array(
            ":identifier" => $identifier
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new RememberTokenException(_("Token not found"), RememberTokenException::NOT_FOUND);
        }

        return (new RememberTokenEntity())
            ->setIdToken((int) $result->id_token)
            ->setIdUser((int) $result->id_user)
            ->setIdentifier($result->identifier)
            ->setRevoked((bool) $result->revoked);
    }

    public function checkHash(RememberTokenEntity $token, string $hash): bool {
        try {
            if ($this->isRevoked($token)) {
                return false;
            }
        } catch (\Exception $exception) {
            throw $exception;
        }

        $userRepo = new UserRepository();

        try {
            $user = $userRepo->getUserEntityByIdentifier($token->getIdUser());
        } catch (\Exception $exception) {
            throw $exception;
        }

        if (password_verify($token->getIdentifier() . $user->getEmailaddress() . $token->getIdentifier(), $hash)) {
            return true;
        }

        $stmt = Db::getPdo()->prepare("
            UPDATE
                remember_token
            SET
                revoked = 1
            WHERE
                id_user = :id_user
        ");

        if (!$stmt->execute(array(
            ":id_user" => $token->getIdUser()
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        return false;
    }

    public function revokeByUser(UserEntity $user) {
        $stmt = Db::getPdo()->prepare("
            UPDATE
                remember_token
            SET
                revoked = 1
            WHERE
                id_user = :id_user
        ");

        if (!$stmt->execute(array(
            ":id_user" => $user->getIdentifier()
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }
    }

    private function getUniqueToken(): string {
        $identifier = bin2hex(openssl_random_pseudo_bytes(16));

        $stmt = Db::getPdo()->prepare("
            SELECT
                COUNT(*) AS total
            FROM
                remember_token
            WHERE
                identifier = :identifier
        ");

        if (!$stmt->execute(array(
            ":identifier" => $identifier
        ))) {
            throw new RememberTokenException(implode(PHP_EOL, $stmt->errorInfo()), RememberTokenException::DB_ERROR);
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return $identifier;
        }

        if ($result->total < 1) {
            return $identifier;
        }

        return $this->getUniqueToken();
    }
}