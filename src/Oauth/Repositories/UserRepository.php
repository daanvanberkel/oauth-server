<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use Daanvanberkel\Oauth\Entities\ClientEntity;
use Daanvanberkel\Oauth\Exceptions\UserException;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Daanvanberkel\Oauth\Entities\UserEntity;

/**
 * Class UserRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class UserRepository implements UserRepositoryInterface {
    private $pdo;

    public function __construct() {
        $this->pdo = Db::getPdo();
    }

    /**
     * Get user based in username/email and password
     *
     * @param string                     $username          Username or email
     * @param string                     $password          User password
     * @param null                       $grantType
     * @param ClientEntityInterface|ClientEntity|null $clientEntity
     *
     * @return UserEntity
     * @throws UserException
     * @throws \Exception
     */
    public function getUserEntityByUserCredentials($username, $password, $grantType = null, ClientEntityInterface $clientEntity = null): UserEntity {
        if (!empty($grantType)) {
            if ($grantType != $clientEntity->getGrant()) {
                throw new \Exception(_("Grant not supported for client"));
            }
        }

        $stmt = $this->pdo->prepare("SELECT id_user, username, firstname, lastname, secret, password, email, twofactor_secret FROM user WHERE username = :username OR email = :email LIMIT 1");
        if (!$stmt->execute(array(":username" => $username, ":email" => $username))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new UserException(_("User not found"), UserException::NOT_FOUND);
        }

        if (!password_verify($password . $result->secret, $result->password)) {
            throw new UserException(_("Password not correct"), UserException::CREDENTIALS_NOT_VALID);
        }

        $userEntity = new UserEntity();
        $userEntity
            ->setIdentifier($result->id_user)
            ->setEmailaddress($result->email)
            ->setTwofactorSecret($result->twofactor_secret)
            ->setFirstname($result->firstname)
            ->setLastname($result->lastname);

        return $userEntity;
    }

    /**
     * Get user by username and email
     *
     * @param string $username
     * @param string $email
     *
     * @return UserEntity
     * @throws UserException
     * @throws \Exception
     */
    public function getUserEntityByUsernameAndEmail(string $username, string $email) {
        $stmt = $this->pdo->prepare("SELECT id_user, username, firstname, lastname, email, twofactor_secret FROM user WHERE username = :username AND email = :email");
        if (!$stmt->execute(array(":username" => $username, ":email" => $email))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new UserException(_("User not found"), UserException::NOT_FOUND);
        }

        $userEntity = new UserEntity();
        $userEntity
            ->setIdentifier($result->id_user)
            ->setEmailaddress($result->email)
            ->setTwofactorSecret($result->twofactor_secret)
            ->setFirstname($result->firstname)
            ->setLastname($result->lastname);

        return $userEntity;
    }

    /**
     * Check if username is in use
     *
     * @param string $username
     *
     * @return bool
     * @throws UserException
     * @throws \Exception
     */
    public function isUsernameInUse(string $username): bool {
        $stmt = $this->pdo->prepare("SELECT id_user, username FROM user WHERE username = :username");
        if (!$stmt->execute(array(":username" => $username))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return false;
        }

        return true;
    }

    /**
     * Check if email is in use
     *
     * @param string $email
     *
     * @return bool
     * @throws UserException
     * @throws \Exception
     */
    public function isEmailInUse(string $email): bool {
        $stmt = $this->pdo->prepare("SELECT id_user, email FROM user WHERE email = :email");
        if (!$stmt->execute(array(":email" => $email))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return false;
        }

        return true;
    }

    /**
     * Get user by identifier
     *
     * @param int $id_user
     *
     * @return UserEntity
     * @throws UserException
     * @throws \Exception
     */
    public function getUserEntityByIdentifier(int $id_user) {
        $stmt = $this->pdo->prepare("SELECT id_user, username, firstname, lastname, email, twofactor_secret FROM user WHERE id_user = :id_user");
        if (!$stmt->execute(array(":id_user" => $id_user))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new UserException(_("User not found"), UserException::NOT_FOUND);
        }

        $userEntity = new UserEntity();
        $userEntity
            ->setIdentifier($result->id_user)
            ->setEmailaddress($result->email)
            ->setTwofactorSecret($result->twofactor_secret)
            ->setFirstname($result->firstname)
            ->setLastname($result->lastname);

        return $userEntity;
    }

    /**
     * Change user password
     *
     * @param UserEntity $user
     * @param string     $password
     *
     * @return bool
     * @throws UserException
     */
    public function changePassword(UserEntity $user, string $password): bool {
        $secret = bin2hex(openssl_random_pseudo_bytes(16));
        $hash = password_hash($password . $secret, PASSWORD_DEFAULT);

        $stmt = $this->pdo->prepare("UPDATE user SET secret = :secret, password = :password WHERE id_user = :id_user");
        if (!$stmt->execute(array(
            ":secret" => $secret,
            ":password" => $hash,
            ":id_user" => $user->getIdentifier()
        ))) {
            throw new UserException(implode(PHP_EOL, $stmt->errorInfo()));
        }

        return true;
    }

    /**
     * Create a new user
     *
     * @param string $username      Username
     * @param string $email         E-mailadres
     * @param string $password      Password
     *
     * @return UserEntity
     * @throws \Exception
     */
    public function newUser(string $username, string $email, string $password, string $firstname, string $lastname): UserEntity {
        // Generate random token
        $secret = bin2hex(openssl_random_pseudo_bytes(16));

        // Append secret to password and hash password
        $hash = password_hash($password . $secret, PASSWORD_DEFAULT);

        // Save user data to database
        $stmt = $this->pdo->prepare("INSERT INTO user (username, firstname, lastname, email, secret, password) VALUES (:username, :firstname, :lastname, :email, :secret, :password)");
        if (!$stmt->execute(array(
            ":username" => $username,
            ":firstname" => $firstname,
            ":lastname" => $lastname,
            ":email" => $email,
            ":secret" => $secret,
            ":password" => $hash
        ))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $user = new UserEntity();
        $user
            ->setIdentifier($this->pdo->lastInsertId())
            ->setEmailaddress($email)
            ->setFirstname($firstname)
            ->setLastname($lastname);

        $email = sprinf(_("
Dear %s,\n
\n
Your account at oauth.daanvanberkel.nl is successful created.\n
\n
Username: %s\n
Email address: %s\n
\n
Yours sincerely,\n
\n
Team oauth.daanvanberkel.nl service\n
"), $user->getName(), $username, $email);

        // Send mail to user
        mail($user->getName() . "<" . $user->getEmailaddress() . ">", _("New account"), $email);

        return $user;
    }

    /**
     * Save new two factor secret to user in the database
     *
     * @param UserEntity $user
     * @param string     $secret
     *
     * @return UserEntity
     * @throws \Exception
     */
    public function saveNewTwofactorSecret(UserEntity $user, string $secret): UserEntity {
        $stmt = $this->pdo->prepare("UPDATE user SET twofactor_secret = :secret WHERE id_user = :identifier");
        if (!$stmt->execute(array(":secret" => $secret, ":identifier" => $user->getIdentifier()))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $user->setTwofactorSecret($secret);

        return $user;
    }
}
