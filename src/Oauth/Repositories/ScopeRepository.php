<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Daanvanberkel\Oauth\Entities\ScopeEntity;

/**
 * Class ScopeRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class ScopeRepository implements ScopeRepositoryInterface {
    /**
     * Get scope by identifier
     *
     * @param string $scopeIdentifier
     *
     * @return ScopeEntity
     */
    public function getScopeEntityByIdentifier($scopeIdentifier) {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT identifier, description FROM scope WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $scopeIdentifier))) {
            return;
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return;
        }

        $scope = new ScopeEntity();
        $scope->setIdentifier($scopeIdentifier);
        $scope->setDescription($result->description);

        return $scope;
    }

    /**
     * The scope list can be changed here
     *
     * @param array                 $scopes
     * @param string                $grantType
     * @param ClientEntityInterface $clientEntity
     * @param null                  $userIdentifier
     *
     * @return array
     */
    public function finalizeScopes(array $scopes, $grantType, ClientEntityInterface $clientEntity, $userIdentifier = null): array {
        return $scopes;
    }
}
