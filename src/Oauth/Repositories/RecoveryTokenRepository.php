<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use Daanvanberkel\Oauth\Entities\RecoveryTokenEntity;
use Daanvanberkel\Oauth\Exceptions\RecoveryTokenException;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;

/**
 * Class RecoveryTokenRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RecoveryTokenRepository {
    private $pdo;

    public function __construct() {
        $this->pdo = Db::getPdo();
    }

    /**
     * Save new recovery token to the database
     *
     * @param AuthorizationRequest $authRequest
     * @param UserEntityInterface  $userEntity
     *
     * @return RecoveryTokenEntity
     * @throws \Exception
     */
    public function saveNewToken(AuthorizationRequest $authRequest, UserEntityInterface $userEntity): RecoveryTokenEntity {
        $userIdentifier = $userEntity->getIdentifier();
        $identifier = $this->_getUniqueToken();

        $recoveryTokenEntity = new RecoveryTokenEntity();
        $recoveryTokenEntity
            ->setAuthRequest($authRequest)
            ->setUserIdentifier($userIdentifier)
            ->setIdentifier($identifier)
            ->setExpiredDate(new \DateTime("+1 day"));

        $stmt = $this->pdo->prepare("
            INSERT INTO 
                recoverytoken (
                    identifier,
                    auth_request,
                    expire_date,
                    revoked,
                    user_identifier
                )
            VALUES (
                :identifier,
                :auth_request,
                :expire_date,
                :revoked,
                :user_identifier
            )
        ");

        if (!$stmt->execute(array(
            ":identifier" => $recoveryTokenEntity->getIdentifier(),
            ":auth_request" => serialize($recoveryTokenEntity->getAuthRequest()),
            ":expire_date" => $recoveryTokenEntity->getExpireDate()->format('Y-m-d H:i:s'),
            ":revoked" => (int) $recoveryTokenEntity->isRevoked(),
            ":user_identifier" => $recoveryTokenEntity->getUserIdentifier()
        ))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        return $recoveryTokenEntity;
    }

    /**
     * Get token from the database
     *
     * @param string $identifier        Token identifier
     *
     * @return RecoveryTokenEntity
     * @throws RecoveryTokenException
     * @throws \Exception
     */
    public function getToken(string $identifier): RecoveryTokenEntity {
        $stmt = $this->pdo->prepare("SELECT identifier, auth_request, expire_date, revoked, user_identifier FROM recoverytoken WHERE identifier = :identifier");
        if (!$stmt->execute(array(
            ":identifier" => $identifier
        ))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            throw new RecoveryTokenException(_("Token not found"), RecoveryTokenException::NOT_FOUND);
        }

        $recoveryTokenEntity = new RecoveryTokenEntity();
        $recoveryTokenEntity
            ->setIdentifier($result->identifier)
            ->setAuthRequest($result->auth_request)
            ->setExpiredDate($result->expire_date)
            ->setRevoked($result->revoked)
            ->setUserIdentifier($result->user_identifier);

        return $recoveryTokenEntity;
    }

    /**
     * Revoke token in the database
     *
     * @param RecoveryTokenEntity $recoveryTokenEntity
     *
     * @return RecoveryTokenEntity
     * @throws \Exception
     */
    public function revoke(RecoveryTokenEntity $recoveryTokenEntity): RecoveryTokenEntity {
        $recoveryTokenEntity->setRevoked(true);

        $stmt = $this->pdo->prepare("UPDATE recoverytoken SET revoked = 1 WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $recoveryTokenEntity->getIdentifier()))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        return $recoveryTokenEntity;
    }

    /**
     * Get an unique token
     *
     * @return string
     * @throws \Exception
     */
    private function _getUniqueToken(): string {
        $identifier = bin2hex(openssl_random_pseudo_bytes(16));

        $stmt = $this->pdo->prepare("SELECT COUNT(*) AS total FROM refreshtoken WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $identifier))) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result->total > 0) {
            return $this->_getUniqueToken();
        }

        return $identifier;
    }
}