<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use Daanvanberkel\Oauth\Exceptions\LoginTriesException;

/**
 * Class LoginTriesRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class LoginTriesRepository {
    /**
     * Add try to database
     *
     * @param null|string $email
     * @throws LoginTriesException
     */
    public function addTry(?string $email = null): void {
        $stmt = Db::getPdo()->prepare("INSERT INTO login_tries (ip, user) VALUES (:ip, :user)");

        if (!$stmt->execute(array(
            ":ip" => $this->get_client_ip(),
            ":user" => $email
        ))) {
            throw new LoginTriesException(implode(PHP_EOL, $stmt->errorInfo()), LoginTriesException::DB_ERROR);
        }
    }

    /**
     * Get total try count
     *
     * @param null|string $ip
     *
     * @return int
     * @throws LoginTriesException
     */
    public function getTriesCount(?string $ip = null): int {
        if (is_null($ip)) {
            $ip = $this->get_client_ip();
        }

        $stmt = Db::getPdo()->prepare("
            SELECT 
                COUNT(*) as tries
            FROM 
                login_tries 
            WHERE 
                (date BETWEEN DATE_SUB(NOW(), INTERVAL 1 HOUR) AND NOW()) AND 
                ip = :ip
        ");

        if (!$stmt->execute(array(
            ":ip" => $ip
        ))) {
            throw new LoginTriesException(implode(PHP_EOL, $stmt->errorInfo()), LoginTriesException::DB_ERROR);
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        return (int) $result->tries;
    }

    /**
     * Get client ip-address
     *
     * @return string
     */
    private function get_client_ip(): string {
        if (getenv('HTTP_CLIENT_IP'))           $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR')) $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))     $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))   $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))       $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))          $ipaddress = getenv('REMOTE_ADDR');
        else                                    $ipaddress = '0.0.0.0';
        return $ipaddress;
    }
}