<?php
namespace Daanvanberkel\Oauth\Repositories;

use Daanvanberkel\Db;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Daanvanberkel\Oauth\Entities\RefreshTokenEntity;

/**
 * Class RefreshTokenRepository
 * @package     Daanvanberkel\Oauth\Repositories
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class RefreshTokenRepository implements RefreshTokenRepositoryInterface {
    /**
     * Save refresh token in the database
     *
     * @param RefreshTokenEntityInterface $refreshTokenEntityInterface
     *
     * @return RefreshTokenEntityInterface
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntityInterface) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("INSERT INTO refreshtoken (identifier, id_accesstoken, expire_date) VALUES (:identifier, :id_accesstoken, :expire_date)");
        if (!$stmt->execute(array(
            ":identifier" => $refreshTokenEntityInterface->getIdentifier(),
            ":id_accesstoken" => $refreshTokenEntityInterface->getAccessToken()->getIdentifier(),
            ":expire_date" => $refreshTokenEntityInterface->getExpiryDateTime()->format('Y-m-d H:i:s')
        ))) {
            return;
        }

        return $refreshTokenEntityInterface;
    }

    /**
     * Revoke refresh token in the database
     *
     * @param string $tokenId
     *
     * @return string
     */
    public function revokeRefreshToken($tokenId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("UPDATE refreshtoken SET revoked = 1 WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $tokenId))) {
            return;
        }

        return $tokenId;
    }

    /**
     * Check if the refresh token is revoked
     *
     * @param string $tokenId
     *
     * @return boolean
     */
    public function isRefreshTokenRevoked($tokenId) {
        $pdo = Db::getPdo();

        $stmt = $pdo->prepare("SELECT expire_date, revoked FROM refreshtoken WHERE identifier = :identifier");
        if (!$stmt->execute(array(":identifier" => $tokenId))) {
            return;
        }

        $result = $stmt->fetch(\PDO::FETCH_OBJ);

        if ($result === false) {
            return true;
        }

        $expire = new \DateTime($result->expire_date);
        $now = new \DateTime();

        return (bool) (($now > $expire) ? true : $result->revoked);
    }

    /**
     * Get new refresh token entity
     *
     * @return RefreshTokenEntity
     */
    public function getNewRefreshToken(): RefreshTokenEntity {
        return new RefreshTokenEntity();
    }
}
