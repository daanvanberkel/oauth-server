<?php
namespace Daanvanberkel;

use Dotenv\Dotenv;

class EnvVariables {
    private static $instance;

    public static function getInstance(): Dotenv {
        if (!isset(self::$instance) || !(self::$instance instanceof Dotenv)) {
            self::$instance = new Dotenv(getcwd());

            self::$instance->load();

            self::$instance->required(array(
                "MYSQL_HOST",
                "MYSQL_USER",
                "MYSQL_PASS",
                "MYSQL_DB",
                "DEFAULT_TITLE",
                "TITLE_SUFFIX",
                "ENABLE_REGISTRATION",
                "ENABLE_PASSWORD_RECOVERY"
            ));
        }

        return self::$instance;
    }
}