<?php
namespace Daanvanberkel;

class Translator {
    public function __construct() {
        $locale = \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']) . ".UTF-8";

        putenv('LC_ALL=' . $locale);

        if (!setlocale(LC_ALL, $locale)) {
            // Fallback to english
            setlocale(LC_ALL, "en_US.UTF-8");
        }

        bindtextdomain("oauthServer", getcwd() . '/languages');
        textdomain("oauthServer");
    }
}