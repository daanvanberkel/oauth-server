<?php
namespace Daanvanberkel\Traits;

use Slim\Container;

trait Endpoint {
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Check is the authrequest object is saved in the session
     *
     * @param bool $userRequired        Must the user be available in the authrequest?
     *
     * @return bool
     */
    private function _checkAuthrequest($userRequired = true): bool {
        // Check if user entered website via /autorize url
        if (!isset($_SESSION['authRequest'])) {
            return false;
        }

        $authRequest = unserialize($_SESSION['authRequest']);

        if ($userRequired && empty($authRequest->getUser())) {
            return false;
        }

        return true;
    }
}