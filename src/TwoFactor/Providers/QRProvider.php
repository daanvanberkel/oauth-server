<?php
namespace Daanvanberkel\TwoFactor\Providers;

use Endroid\QrCode\QrCode;
use RobThree\Auth\Providers\Qr\IQRCodeProvider;

/**
 * Class QRProvider
 * @package     Daanvanberkel\TwoFactor\Providers
 * @implements  IQRCodeProvider
 *
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class QRProvider implements IQRCodeProvider {
    /**
     * Generate QR-code
     *
     * @param $qrtext
     * @param $size
     *
     * @return string
     */
    public function getQRCodeImage($qrtext, $size) {
        $qrCode = new QrCode();
        $qrCode
            ->setText($qrtext)
            ->setSize($size)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(array("r" => 0, "g" => 0, "b" => 0))
            ->setBackgroundColor(array("r" => 255, "g" => 255, "b" => 255))
            ->setImageType('png');

        return $qrCode->get();
    }

    /**
     * Get QR-code mime type
     *
     * @return string
     */
    public function getMimeType() {
        return 'image/png';
    }
}