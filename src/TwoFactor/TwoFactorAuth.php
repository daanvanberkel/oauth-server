<?php
namespace Daanvanberkel\TwoFactor;

use Daanvanberkel\TwoFactor\Providers\QRProvider;

/**
 * Class TwoFactorAuth
 * @package     Daanvanberkel\TwoFactor
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */
class TwoFactorAuth {
    private static $instance;

    /**
     * Get the two factor auth instance (singleton)
     *
     * @return \RobThree\Auth\TwoFactorAuth
     */
    public static function getInstance(): \RobThree\Auth\TwoFactorAuth {
        if (!isset(self::$instance)) {
            self::$instance = new \RobThree\Auth\TwoFactorAuth(_('OAuth service'), 6, 30, 'sha1', new QRProvider());
        }

        return self::$instance;
    }
}