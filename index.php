<?php
/**
 * OAuth server
 *
 * @author      Daan van Berkel <info@daanvanberkel.nl>
 * @license     MIT
 */

// Display errors
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Start session
session_start();

// Check if vendor/autoload.php exists
if (!file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
    die("Did you forget to run <code>composer install</code>?");
}

define("COOKIE_REMEMBER_IDENTIFIER", "BDrh2Q6LeL");
define("COOKIE_REMEMBER_HASH", "enZ1mivvo4");

// Composer autoload
require_once(__DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

// Init translator
$translator = new \Daanvanberkel\Translator();

// Get AuthorizationServer instance
$server = \Daanvanberkel\AuthorizationServer::getInstance();

// Get Dotenv instance
$dotenv = \Daanvanberkel\EnvVariables::getInstance();

// Get Slim application instance
$app = new \Slim\App();

$container = $app->getContainer();
// Add php view to Slim container
$container['renderer'] = new \Slim\Views\PhpRenderer(__DIR__ . '/templates', array("title" => getenv("DEFAULT_TITLE"), "title_suffix" => " - " . getenv("TITLE_SUFFIX")));
// Add oauth server to Slim container
$container['oauth_server'] = $server;

// Get requests
$app->get('/authorize', \Daanvanberkel\Endpoints\Authorize::class . ':get');
$app->get('/login', \Daanvanberkel\Endpoints\Login::class . ':get');
$app->get('/approve', \Daanvanberkel\Endpoints\Approve::class . ':get');
$app->get('/recovery', \Daanvanberkel\Endpoints\Recovery::class . ':get');
$app->get('/recovery/{token:[a-zA-Z0-9]+}', \Daanvanberkel\Endpoints\Recovery::class . ':changePasswordGet');
$app->get('/twofactor', \Daanvanberkel\Endpoints\Twofactor::class . ':get');
$app->get('/twofactor/setup', \Daanvanberkel\Endpoints\Twofactor\Setup::class . ':get');
$app->get('/register', \Daanvanberkel\Endpoints\Register::class . ':get');
$app->get('/revoked/{id:[a-zA-Z0-9]+}', \Daanvanberkel\Endpoints\Revoked::class . ':get');
$app->get('/logout', \Daanvanberkel\Endpoints\Logout::class . ":get");

// Post requests
$app->post('/login', \Daanvanberkel\Endpoints\Login::class . ':post');
$app->post('/approve', \Daanvanberkel\Endpoints\Approve::class . ':post');
$app->post('/access_token', \Daanvanberkel\Endpoints\AccessToken::class . ':post');
$app->post('/recovery', \Daanvanberkel\Endpoints\Recovery::class . ':post');
$app->post('/recovery/process', \Daanvanberkel\Endpoints\Recovery::class . ':changePasswordPost');
$app->post('/twofactor', \Daanvanberkel\Endpoints\Twofactor::class . ':post');
$app->post('/twofactor/setup', \Daanvanberkel\Endpoints\Twofactor\Setup::class . ':post');
$app->post('/register', \Daanvanberkel\Endpoints\Register::class . ':post');

// Secured requests
$app->group('/secure', function() {
    $this->get('/user', \Daanvanberkel\Endpoints\Secure\User::class . ':get');
    $this->get('/checkaccesstoken', \Daanvanberkel\Endpoints\Secure\CheckAccessToken::class . ':get');
})
    ->add(new \League\OAuth2\Server\Middleware\ResourceServerMiddleware(
        new \League\OAuth2\Server\ResourceServer(
            \Daanvanberkel\AuthorizationServer::getAccessTokenRepository(),
            \Daanvanberkel\AuthorizationServer::getPublicKey())
        )
    );

// Run slim app
$app->run();
