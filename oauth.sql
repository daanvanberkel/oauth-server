-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 08 jan 2017 om 11:23
-- Serverversie: 5.7.16-0ubuntu0.16.04.1
-- PHP-versie: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oauth`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `accesstoken`
--

CREATE TABLE `accesstoken` (
  `identifier` varchar(255) NOT NULL,
  `id_client` varchar(30) NOT NULL,
  `id_user` int(11) NOT NULL,
  `expirydatetime` datetime NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `accesstoken_scopes`
--

CREATE TABLE `accesstoken_scopes` (
  `id_accestoken_scope` int(11) NOT NULL,
  `id_accesstoken` varchar(255) NOT NULL,
  `id_scope` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `authcode`
--

CREATE TABLE `authcode` (
  `identifier` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_client` varchar(30) NOT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `authcode_scopes`
--

CREATE TABLE `authcode_scopes` (
  `id_authcode_scope` int(11) NOT NULL,
  `id_authcode` varchar(255) NOT NULL,
  `id_scope` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `client`
--

CREATE TABLE `client` (
  `identifier` varchar(30) NOT NULL,
  `secret` text NOT NULL,
  `name` varchar(30) NOT NULL,
  `redirect_url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `recoverytoken`
--

CREATE TABLE `recoverytoken` (
  `identifier` varchar(32) NOT NULL,
  `auth_request` text NOT NULL,
  `expire_date` datetime NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `user_identifier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `refreshtoken`
--

CREATE TABLE `refreshtoken` (
  `identifier` varchar(255) NOT NULL,
  `id_accesstoken` varchar(255) NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `remember_token`
--

CREATE TABLE `remember_token` (
  `id_token` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `identifier` varchar(40) NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `scope`
--

CREATE TABLE `scope` (
  `identifier` varchar(30) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `secret` text NOT NULL,
  `password` text NOT NULL,
  `twofactor_secret` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `accesstoken`
--
ALTER TABLE `accesstoken`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexen voor tabel `accesstoken_scopes`
--
ALTER TABLE `accesstoken_scopes`
  ADD PRIMARY KEY (`id_accestoken_scope`),
  ADD KEY `id_accesstoken` (`id_accesstoken`),
  ADD KEY `id_scope` (`id_scope`);

--
-- Indexen voor tabel `authcode`
--
ALTER TABLE `authcode`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `client_identifier` (`id_client`);

--
-- Indexen voor tabel `authcode_scopes`
--
ALTER TABLE `authcode_scopes`
  ADD PRIMARY KEY (`id_authcode_scope`),
  ADD KEY `id_authcode` (`id_authcode`),
  ADD KEY `id_scope` (`id_scope`);

--
-- Indexen voor tabel `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexen voor tabel `recoverytoken`
--
ALTER TABLE `recoverytoken`
  ADD PRIMARY KEY (`identifier`),
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Indexen voor tabel `refreshtoken`
--
ALTER TABLE `refreshtoken`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `id_accesstoken` (`id_accesstoken`);

--
-- Indexen voor tabel `remember_token`
--
ALTER TABLE `remember_token`
  ADD PRIMARY KEY (`id_token`),
  ADD UNIQUE KEY `identifier` (`identifier`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexen voor tabel `scope`
--
ALTER TABLE `scope`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `accesstoken_scopes`
--
ALTER TABLE `accesstoken_scopes`
  MODIFY `id_accestoken_scope` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT voor een tabel `authcode_scopes`
--
ALTER TABLE `authcode_scopes`
  MODIFY `id_authcode_scope` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT voor een tabel `remember_token`
--
ALTER TABLE `remember_token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `accesstoken`
--
ALTER TABLE `accesstoken`
  ADD CONSTRAINT `accesstoken_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `accesstoken_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `accesstoken_scopes`
--
ALTER TABLE `accesstoken_scopes`
  ADD CONSTRAINT `accesstoken` FOREIGN KEY (`id_accesstoken`) REFERENCES `accesstoken` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scope` FOREIGN KEY (`id_scope`) REFERENCES `scope` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `authcode`
--
ALTER TABLE `authcode`
  ADD CONSTRAINT `authcode_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authcode_ibfk_2` FOREIGN KEY (`id_client`) REFERENCES `client` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `authcode_scopes`
--
ALTER TABLE `authcode_scopes`
  ADD CONSTRAINT `authcode_scopes_ibfk_1` FOREIGN KEY (`id_authcode`) REFERENCES `authcode` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authcode_scopes_ibfk_2` FOREIGN KEY (`id_scope`) REFERENCES `scope` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `refreshtoken`
--
ALTER TABLE `refreshtoken`
  ADD CONSTRAINT `refreshtoken_ibfk_1` FOREIGN KEY (`id_accesstoken`) REFERENCES `accesstoken` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `remember_token`
--
ALTER TABLE `remember_token`
  ADD CONSTRAINT `remember_token_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
