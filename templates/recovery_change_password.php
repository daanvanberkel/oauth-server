<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Forgot your password"); ?></h1>

    <?php if(!empty($error)): ?>
        <div class="warning">
            <?php
            switch ($error) {
                case \Daanvanberkel\Endpoints\Recovery::CREDENTIALS_NOT_VALID:
                    echo _("Username and password are required");
                    break;

                case \Daanvanberkel\Endpoints\Recovery::MAIL_FAILED:
                    echo _("Sending the instruction email failed. Try again later");
                    break;
            }
            ?>
        </div>
    <?php endif; ?>

    <form action="/recovery/process" method="post" class="form">
        <input type="hidden" name="token" value="<?php echo $token; ?>" />
        <input type="password" name="password1" placeholder="<?php echo _("Password"); ?>" class="control" autofocus />
        <input type="password" name="password2" placeholder="<?php echo _("Repeat password"); ?>" class="control" />
        <button type="submit"><?php echo _("Change password"); ?></button>
    </form>
<?php include('base/footer.php'); ?>