<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Forgot your password"); ?></h1>

    <?php if(!empty($error)): ?>
        <div class="warning">
            <?php
            switch ($error) {
                case \Daanvanberkel\Endpoints\Recovery::CREDENTIALS_NOT_VALID:
                    echo _("Username and password are required");
                    break;

                case \Daanvanberkel\Endpoints\Recovery::MAIL_FAILED:
                    echo _("Sending the instruction email failed. Try again later");
                    break;
            }
            ?>
        </div>
    <?php endif; ?>

    <form action="/recovery" method="post" class="form">
        <input type="text" name="username" placeholder="<?php echo _("Username"); ?>" class="control" autofocus />
        <input type="email" name="email" placeholder="<?php echo _("Email address"); ?>" class="control" />
        <button type="submit"><?php echo _("Request password"); ?></button>
    </form>

    <div class="actions-container">
        <div class="action-left">
            <a href="/login"><?php echo _("Back"); ?></a>
        </div>
    </div>
<?php include('base/footer.php'); ?>