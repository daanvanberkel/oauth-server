<?php
$show_sign_off = false;

if (isset($_COOKIE[COOKIE_REMEMBER_IDENTIFIER]) && isset($_COOKIE[COOKIE_REMEMBER_HASH])) {
    $show_sign_off = true;
}
?>
    </div>
</div>
<footer>
    <div id="copyright-container">&copy; <?php echo (date('Y') > 2016 ? '2016 - ' . date('Y') : date('Y')); ?> <a href="http://www.daanvanberkel.nl" target="_blank">Daan van Berkel</a><?php if($show_sign_off): ?> - <a href="/logout"><?php echo _("Sign out"); ?></a><?php endif;?></div>
</footer>
<script type="text/javascript" src="https://jira.daanvanberkel.nl/s/e2d011f27e40602e299e14fc045b3e35-T/o9lkrr/72006/b6b48b2829824b869586ac216d119363/2.0.22/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=nl-NL&collectorId=062ef315"></script>
</body>
</html>
