<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Two factor authentication"); ?></h1>

<?php if(!empty($error)): ?>
    <div class="warning">
        <?php
        switch ($error) {
            case \Daanvanberkel\Endpoints\Recovery::CREDENTIALS_NOT_VALID:
                echo _("Username and password are required");
                break;

            case \Daanvanberkel\Endpoints\Recovery::MAIL_FAILED:
                echo _("Sending the instruction email failed. Try again later");
                break;
        }
        ?>
    </div>
<?php endif; ?>

    <p><?php echo _("Two factor authentication is not configured yet. Would you like to configure two factor authentication?"); ?></p>

    <div class="actions-container">
        <div class="action-left">
            <a href="/twofactor/setup"><?php echo _("Yes"); ?></a>
        </div>

        <div class="action-right">
            <a href="/approve"><?php echo _("No"); ?></a>
        </div>
    </div>
<?php include('base/footer.php'); ?>