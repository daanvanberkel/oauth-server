<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="https://www.gravatar.com/avatar/<?php echo md5($user->getEmailaddress()); ?>" alt="<?php echo _('avatar'); ?>" />
    </div>

    <p><?php echo sprintf(_("&quot;%s&quot; requested the following data:"), $application->getName()); ?></p>

    <ul class="scope-list">
        <?php foreach($scopes as $scope): ?>
            <li><?php echo $scope->getDescription(); ?></li>
        <?php endforeach; ?>
    </ul>

    <form action="/approve" method="post" class="form">
        <button type="submit" name="action" value="approve"><?php echo _("Approve"); ?></button>
        <button type="submit" name="action" value="deny"><?php echo _("Deny"); ?></button><br/><br/>

        <label><input type="checkbox" name="approve" value="true" /> <?php echo _("Remember my choice"); ?></label>
    </form>
<?php include('base/footer.php'); ?>