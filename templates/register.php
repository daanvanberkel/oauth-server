<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Register"); ?></h1>

    <?php if(!empty($error)): ?>
        <div class="warning">
            <?php
            switch ($error) {
                case \Daanvanberkel\Endpoints\Register::DATA_MISSING:
                    echo _("All fields are required");
                    break;

                case \Daanvanberkel\Endpoints\Register::EMAIL_NOT_VALID:
                    echo _("The submitted email address is not valid");
                    break;

                case \Daanvanberkel\Endpoints\Register::PASSWORD_MISMATCH:
                    echo _("The password don't match");
                    break;

                case \Daanvanberkel\Endpoints\Register::USERNAME_OR_EMAIL_IN_USE:
                    echo _("The username or email address is already in use");
                    break;
            }
            ?>
        </div>
    <?php endif; ?>

    <form action="/register" method="post" class="form">
        <input type="text" name="username" placeholder="<?php echo _("Username"); ?>" class="control" autofocus />
        <input type="text" name="firstname" placeholder="<?php echo _("Firstname"); ?>" class="control" />
        <input type="text" name="lastname" placeholder="<?php echo _("Lastname"); ?>" class="control" />
        <input type="email" name="email" placeholder="<?php echo _("Email address"); ?>" class="control" />
        <input type="password" name="password1" placeholder="<?php echo _("Password"); ?>" class="control" />
        <input type="password" name="password2" placeholder="<?php echo _("Repeat password"); ?>" class="control" />
        <button type="submit"><?php echo _("Register"); ?></button>
    </form>

    <div class="actions-container">
        <div class="action-left">
            <a href="/login"><?php echo _("Sign in"); ?></a>
        </div>
    </div>
<?php include('base/footer.php'); ?>