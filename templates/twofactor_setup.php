<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Two factor authentication"); ?></h1>

<?php if(!empty($error)): ?>
    <div class="warning">
        <?php
        switch ($error) {
            case \Daanvanberkel\Endpoints\Twofactor\Setup::CODE_MISSING:
                echo _("Enter a confirm code.");
                break;

            case \Daanvanberkel\Endpoints\Twofactor\Setup::CODE_NOT_VALID:
                echo _("The entered confirm code is incorrect, try again.");
                break;

            case \Daanvanberkel\Endpoints\Twofactor\Setup::SECRET_MISSING:
                echo _("An unknown error occured. Please try again.");
                break;
        }
        ?>
    </div>
<?php endif; ?>

    <p><?php echo _("Scan this QR-code with the <a href=\"https://support.google.com/accounts/answer/1066447?hl=nl\">Google Authenticator</a> app"); ?></p>

    <p>
        <img src="<?php echo $qr_code; ?>" alt="QR-Code" /><br/>
        <?php echo $secret; ?>
    </p>

    <p><?php echo _("When you scanned the code and the app is configured, enter the confirm code in the text field below"); ?></p>

    <form action="/twofactor/setup" method="post" class="form">
        <input type="hidden" name="secret" value="<?php echo $secret; ?>" />
        <input type="text" name="code" placeholder="<?php echo _("Confirm code"); ?>" class="control" autofocus />

        <button type="submit"><?php echo _("Save"); ?></button>
    </form>
<?php include('base/footer.php'); ?>