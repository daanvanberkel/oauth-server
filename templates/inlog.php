<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _('Sign in'); ?></h1>

    <?php if(!empty($error)): ?>
        <div class="warning">
            <?php
            switch ($error) {
                case \Daanvanberkel\Oauth\Exceptions\UserException::NOT_FOUND:
                    echo sprintf(_("User not found. (%d more tries and your ip will be blocked for one hour)"), (2 - $tries));
                    break;

                case \Daanvanberkel\Oauth\Exceptions\UserException::CREDENTIALS_NOT_VALID:
                    echo sprintf(_("Password not correct. (%d more tries and your op will be blocked for one hour)"), (2 - $tries));
                    break;

                case \Daanvanberkel\Oauth\Exceptions\UserException::BRUTE_FORCE_ERROR:
                    echo _("You have to many incorrect login tries. Your ip is blocked for one hour.");
                    break;
            }
            ?>
        </div>
    <?php endif; ?>

    <?php if(!empty($recovery)): ?>
        <div class="success">
            <?php
            switch($recovery) {
                case \Daanvanberkel\Endpoints\Recovery::MAIL_SEND:
                    echo _("An email is send to your emailadres with instructions to create a new password");
                    break;

                case \Daanvanberkel\Endpoints\Recovery::PASSWORD_CHANGED:
                    echo _("Your password is changed. You can use this password to sign in.");
                    break;
            }
            ?>
        </div>
    <?php endif; ?>

    <?php if(!empty($register) && $register === true): ?>
        <div class="success">
            <?php echo _("Your account is created. You can sign in now."); ?>
        </div>
    <?php endif; ?>

    <?php if (empty($error) || $error !== \Daanvanberkel\Oauth\Exceptions\UserException::BRUTE_FORCE_ERROR): ?>
    <form action="/login" method="post" class="form">
        <input type="text" name="username" placeholder="<?php echo _("Username"); ?>" class="control" value="<?php echo $username; ?>"<?php echo (empty($username) ? ' autofocus' : ''); ?> />
        <input type="password" name="password" placeholder="<?php echo _("Password"); ?>" class="control"<?php echo (!empty($username) ? ' autofocus' : ''); ?> />
        <label><input type="checkbox" name="remember" value="true"<?php echo ($remember ? " checked" : ""); ?> /> <?php echo _("Remember me"); ?></label><br/><br/>
        <button type="submit"><?php echo _("Sign in"); ?></button>
    </form>

    <div class="actions-container">
        <?php if (getenv('ENABLE_PASSWORD_RECOVERY') == 'true'): ?>
            <div class="action-left">
                <a href="/recovery"><?php echo _("Forgot your password?"); ?></a>
            </div>
        <?php endif; ?>

        <?php if (getenv('ENABLE_REGISTRATION') == 'true' && (empty($register) || $register === false)): ?>
            <div class="action-right">
                <a href="/register"><?php echo _("Register"); ?></a>
            </div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
<?php include('base/footer.php'); ?>