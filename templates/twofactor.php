<?php include('base/header.php'); ?>
    <div class="image-container">
        <img src="/assets/images/locked-padlock.svg" alt="lock" />
    </div>

    <h1><?php echo _("Two factor authentication"); ?></h1>

<?php if(!empty($error)): ?>
    <div class="warning">
        <?php
        switch ($error) {
            case \Daanvanberkel\Endpoints\Twofactor::CODE_MISSING:
                echo _("Enter a confirm code.");
                break;

            case \Daanvanberkel\Endpoints\Twofactor::CODE_MISMATCH:
                echo _("The entered confirm code is incorrect, try again.");
                break;
        }
        ?>
    </div>
<?php endif; ?>

    <p><?php echo _("Enter the confirm code. You can find the confirm code in the <a href=\"https://support.google.com/accounts/answer/1066447?hl=nl\">Google Authenticator</a> app."); ?></p>

    <form action="/twofactor" method="post" class="form">
        <input type="hidden" name="secret" value="<?php echo $secret ?? null; ?>" />
        <input type="number" name="code" placeholder="<?php echo _("Confirm code"); ?>" class="control" autofocus />

        <button type="submit"><?php echo _("Sign in"); ?></button>
    </form>
<?php include('base/footer.php'); ?>