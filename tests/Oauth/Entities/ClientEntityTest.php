<?php

class ClientEntityTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param $method
     * @dataProvider methodsProvider
     */
    public function testMethods($method) {
        $client = new \Daanvanberkel\Oauth\Entities\ClientEntity();

        $this->assertTrue(method_exists($client, $method));
    }

    public function methodsProvider() {
        return array(
            array("getIdentifier"),
            array("setIdentifier"),
            array("getName"),
            array("setName"),
            array("getRedirectUri"),
            array("setRedirectUri"),
            array("getGrant"),
            array("setGrant")
        );
    }
}