<?php

class RefreshTokenEntityTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param $method
     * @dataProvider methodsProvider
     */
    public function testMethods($method) {
        $token = new \Daanvanberkel\Oauth\Entities\RefreshTokenEntity();

        $this->assertTrue(method_exists($token, $method));
    }

    public function methodsProvider() {
        return array(
            array("setAccessToken"),
            array("getAccessToken"),
            array("getExpiryDateTime"),
            array("setExpiryDateTime"),
            array("getIdentifier"),
            array("setIdentifier")
        );
    }
}