<?php

class ScopeEntityTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param $method
     * @dataProvider methodsProvider
     */
    public function testMethods($method) {
        $scope = new \Daanvanberkel\Oauth\Entities\ScopeEntity();

        $this->assertTrue(method_exists($scope, $method));
    }

    public function methodsProvider() {
        return array(
            array("getDescription"),
            array("setDescription"),
            array("jsonSerialize"),
            array("getIdentifier"),
            array("setIdentifier"),
        );
    }
}