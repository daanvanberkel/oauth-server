<?php

class AuthCodeEntityTest extends \PHPUnit\Framework\TestCase {
    private $repo;

    public function setUp() {
        $this->repo = new \Daanvanberkel\Oauth\Repositories\AuthCodeRepository();
    }

    public function testNewAuthCode() {
        $authCode = $this->repo->getNewAuthCode();

        $this->assertInstanceOf(\Daanvanberkel\Oauth\Entities\AuthCodeEntity::class, $authCode);

        return $authCode;
    }

    /**
     * @param $method
     * @param \Daanvanberkel\Oauth\Entities\AuthCodeEntity $authCode
     * @depends      testNewAuthCode
     * @dataProvider methodsProvider
     */
    public function testMethods($method, \Daanvanberkel\Oauth\Entities\AuthCodeEntity $authCode) {
        $this->assertTrue(method_exists($authCode, $method));
    }

    public function methodsProvider() {
        return array(
            array("getIdentifier"),
            array("setIdentifier"),
            array("addScope"),
            array("getScopes"),
            array("getExpiryDateTime"),
            array("setExpiryDateTime"),
            array("getUserIdentifier"),
            array("setUserIdentifier"),
            array("getClient"),
            array("setClient"),
            array("getRedirectUri"),
            array("setRedirectUri")
        );
    }
}