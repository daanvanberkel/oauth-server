<?php

class RecoveryTokenEntityTest extends \PHPUnit\Framework\TestCase {
    /**
     * @dataProvider methodsProvider
     */
    public function testMethods($method) {
        $token = new \Daanvanberkel\Oauth\Entities\RecoveryTokenEntity();

        $this->assertTrue(method_exists($token, $method));
    }

    public function methodsProvider() {
        return array(
            array("getAuthRequest"),
            array("setAuthRequest"),
            array("getIdentifier"),
            array("setIdentifier"),
            array("getExpireDate"),
            array("setExpiredDate"),
            array("isRevoked"),
            array("setRevoked"),
            array("isExpired"),
            array("getUserIdentifier"),
            array("setUserIdentifier")
        );
    }
}