<?php

class AccessTokenEntityTest extends \PHPUnit\Framework\TestCase {
    private $repo;

    public function setUp() {
        $this->repo = new \Daanvanberkel\Oauth\Repositories\AccessTokenRepository();
    }

    public function testNewAccessToken() {
        $client = new \Daanvanberkel\Oauth\Entities\ClientEntity();
        $client->setIdentifier(1);
        $client
            ->setName("webapp")
            ->setRedirectUri('http://example.com/');

        $scopes = array(
            new \Daanvanberkel\Oauth\Entities\ScopeEntity(),
            new \Daanvanberkel\Oauth\Entities\ScopeEntity()
        );

        $scopes[0]->setIdentifier("test1");
        $scopes[0]->setDescription("Test 1");

        $scopes[1]->setIdentifier("test2");
        $scopes[1]->setDescription("Test 2");

        $accessToken = $this->repo->getNewToken($client, $scopes);

        $this->assertInstanceOf(\Daanvanberkel\Oauth\Entities\AccessTokenEntity::class, $accessToken);

        return $accessToken;
    }

    /**
     * @param $method
     * @param \Daanvanberkel\Oauth\Entities\AccessTokenEntity $accessToken
     * @return \Daanvanberkel\Oauth\Entities\AccessTokenEntity
     * @depends      testNewAccessToken
     * @dataProvider methodsProvider
     */
    public function testMethods($method, \Daanvanberkel\Oauth\Entities\AccessTokenEntity $accessToken) {
        $this->assertTrue(method_exists($accessToken, $method));

        return $accessToken;
    }

    public function methodsProvider() {
        return array(
            array("convertToJWT"),
            array("getClient"),
            array("setClient"),
            array("getExpiryDateTime"),
            array("setExpiryDateTime"),
            array("getUserIdentifier"),
            array("setUserIdentifier"),
            array("getScopes"),
            array("addScope"),
            array("getIdentifier"),
            array("setIdentifier")
        );
    }
}