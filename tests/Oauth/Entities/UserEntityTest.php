<?php

class UserEntityTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param $method
     * @dataProvider methodsProvider
     */
    public function testMethods($method) {
        $user = new \Daanvanberkel\Oauth\Entities\UserEntity();

        $this->assertTrue(method_exists($user, $method));
    }

    public function methodsProvider() {
        return array(
            array("getIdentifier"),
            array("setIdentifier"),
            array("getEmailaddress"),
            array("setEmailaddress"),
            array("getTwofactorSecret"),
            array("setTwofactorSecret"),
            array("getTwofactorVerified"),
            array("setTwofactorVerified"),
            array("getFirstname"),
            array("setFirstname"),
            array("getLastname"),
            array("setLastname"),
            array("getName"),
        );
    }
}