<?php

class EnvVariablesTest extends \PHPUnit\Framework\TestCase {
    private $dotenv_file_copied = false;

    public function setUp() {
        if (!file_exists(getcwd() . '/.env') && file_exists(getcwd() . '/example.env')) {
            copy(getcwd() . '/example.env', getcwd() . '/.env');
            $this->dotenv_file_copied = true;
        }
    }

    public function testInstance() {
        $dotenv = \Daanvanberkel\EnvVariables::getInstance();

        $this->assertInstanceOf(\Dotenv\Dotenv::class, $dotenv);

        return $_ENV;
    }

    /**
     * @param $var
     * @param $env
     * @depends testInstance
     * @dataProvider varNamesProvider
     */
    public function testVariables($var, $env) {
        $this->assertArrayHasKey($var, $env);
    }

    /**
     * @return array
     */
    public function varNamesProvider() {
        return array(
            array("MYSQL_HOST"),
            array("MYSQL_USER"),
            array("MYSQL_PASS"),
            array("MYSQL_DB"),
            array("DEFAULT_TITLE"),
            array("TITLE_SUFFIX"),
            array("ENABLE_REGISTRATION"),
            array("ENABLE_PASSWORD_RECOVERY")
        );
    }

    public function tearDown() {
        if ($this->dotenv_file_copied === true) {
            unlink(getcwd() . '/.env');
        }
    }
}