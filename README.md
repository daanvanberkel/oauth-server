# OAuth php server

## Components
This oauth php server project is using the following components:

- [league/oauth2-server](https://github.com/thephpleague/oauth2-server)
- [slim/slim](https://www.slimframework.com/)
- [slim/php-view](https://github.com/slimphp/PHP-View)
- [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv)
- [robthree/twofactorauth](https://github.com/RobThree/TwoFactorAuth)
- [endroid/qrcode](https://github.com/endroid/QrCode)

## Installation
1. Copy `example.env` to `.env`
1. Change variables in `.env` file
1. Import `oauth.sql` into your database
1. Run the `composer install` command
1. Generate a public/private key pair
1. Make sure that the key pair lives in `../oauth/private.key` and `../oauth/public.key`.
1. (optional) To remove revoked or expired tokens from the database you can add a cronjob that runs the `cron` file 

### Generate key pair
To generate the private key run this command on the terminal:

```
openssl genrsa -out private.key 1024
```

then extract the public key from the private key:

```
openssl rsa -in private.key -pubout -out public.key
```

The private key must be kept secret (i.e. out of the web-root of the oauth server).